/*
  getline.c --> function for getting a line of any length
  from a filehandle
  
  Copyright (c) 1998 Manni Wood

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

  Manni Wood: mwood@sig.bsh.com, pq1036@110.net

 */

#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>
#include <errno.h>
#include <sys/types.h>  /* defines pid_t, etc */
#include <glib.h>
#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>  /* contains all keyboard defs */
#include <ctype.h>  /* isdigit() needed by string_contains_all_numbers */
#include "getline.h"

gchar *get_line_safely(FILE *filehandle, gboolean *success) {
    gchar *line = NULL;
    size_t line_length = 0;
    /* number of additional chars to allocate *line when
       it runs out of space */
    const size_t alloc_length = 1024;
    size_t chars_read_from_file_line = 0;
    gchar next_char = '\0';

    while (next_char != '\n' && next_char != EOF) {
	next_char = fgetc(filehandle);
	++chars_read_from_file_line;
	if (next_char != '\n' && next_char != EOF) {
	    if (chars_read_from_file_line > line_length) {
		line = realloc(line,
			       (sizeof(gchar) * line_length) +
			       (sizeof(gchar) * alloc_length) +
			       1);
		/* line will be null if the memory wasn't
		   correctly allocated */
		if (line == NULL) {
		    fprintf(stderr, "out of memory\n");
		    exit(1);
		}
		/* don't forget to keep line_length accurate! We are
		   pointing to line_length as one of this function's
		   arguments, and we will need it later! */
		line_length += alloc_length;
	    }
	    /* finally, we can append the char! */
	    line[chars_read_from_file_line - 1] = next_char;
	    /* don't forget the terminating null, which we have
	       just overwritten with next_char! The math above
	       should always be leaving me enough memory for that
	       one extra null character after next_char */
	    line[chars_read_from_file_line] = '\0';
	}
    }
    /* don't forget to return whether or not we are at EOF */
    *success = (next_char != EOF);
    /* return a pointer to the newly allocated string */
    return line;
}




