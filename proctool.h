#ifndef PROCTOOL_H
#define PROCTOOL_H
/*
  proctool.h --> tool for finding processes on the system
  
  Copyright (c) 1998 Manni Wood

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

  Manni Wood: mwood@sig.bsh.com, pq1036@110.net

 */

#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>
#include <errno.h>
#include <sys/types.h>  /* defines pid_t, etc */
#include <glib.h>

/* will return the pid of the running process named in procname, or 0 if
   the process wasn't found. If an error message occurred, 
   checkerrno will be TRUE, else FALSE. In the event checkerrno
   is TRUE, strerror(errno) can be used to find the exact nature of the
   error. (strerror(errno) is a part of the standard unix libary, and
   errno is a global variable) */
/* notes:
   a pid_t is a positive integer that uniquely identifies a running process
   a uid_t is a positive integer holding a user id */
pid_t process_is_running(const gchar *procname,
			 uid_t user_id,
			 gboolean *checkerrno);

#endif /* ifndef PROCTOOL_H */

/*
  sprintf(error_msg_gstr, "Can't open /proc directory.\n\"%s\"", strerror(errno));

    if (errno) {
	error_msg_gstr = g_string_new("");
	g_string_sprintf(error_msg_gstr, "Error reading the proc directory.\n\"%s\"", strerror(errno));
	show_error_dialog_box(error_msg_gstr->str);
	g_string_free(error_msg_gstr, TRUE);
	return;
    }

*/
