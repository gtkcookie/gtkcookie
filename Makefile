# what base directory do you want for the installation?
INSTPREFIX = /usr/local

# your gcc compiler
CC = /usr/local/bin/gcc

# define macro for gcc flags
CFLAGS := -Wall -D_XOPEN_SOURCE -g $(shell pkg-config --cflags gtk+-2.0) #-D_MANNI_DEBUG
# with optimisation, and without debugging information...
# CFLAGS = -O3 -Wall -D_XOPEN_SOURCE `gtk-config --cflags`

# define libraries to include
LIBRARIES := $(shell pkg-config --libs gtk+-2.0)
# statically link libraries
# LIBRARIES = -static `gtk-config --libs`
# electric fence -- enable static linking to ensure this works
# LIBRARIES = `gtk-config --libs` -lefence

INSTALL = install -o root -g root

HEADERFILES = proctool.h getline.h

# the default target is all:, so that if you just say "make", all: is
# executed

all: gtkcookie

gtkcookie: gtkcookie.o proctool.o getline.o
	$(CC) $(CFLAGS) -o gtkcookie gtkcookie.o proctool.o getline.o $(LIBRARIES)

gtkcookie.o: gtkcookie.c $(HEADERFILES)
	$(CC) $(CFLAGS) -c gtkcookie.c

proctool.o: proctool.c $(HEADERFILES)
	$(CC) $(CFLAGS) -c proctool.c

getline.o: getline.c $(HEADERFILES)
	$(CC) $(CFLAGS) -c getline.c

install: gtkcookie
	$(INSTALL) -d -m 755 $(INSTPREFIX)/bin
	$(INSTALL) -s -m 755 gtkcookie $(INSTPREFIX)/bin
	$(INSTALL) -d -m 755 $(INSTPREFIX)/share/pixmaps
	$(INSTALL)    -m 644 cookie.xpm $(INSTPREFIX)/share/pixmaps
	$(INSTALL)    -m 644 small_cookie.xpm $(INSTPREFIX)/share/pixmaps
	$(INSTALL) -d -m 755 $(INSTPREFIX)/share/man/man1
	$(INSTALL)    -m 644 gtkcookie.1 $(INSTPREFIX)/share/man/man1

clean:
	rm -f gtkcookie
	rm -f *.o
	rm -f *~

