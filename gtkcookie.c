/*

  gtkcookie.c --> gtk+ web browser cookie file editor

  Copyright (c) 1998 Manni Wood

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

  Manni Wood: mwood@sig.bsh.com, pq1036@110.net

  Netscape, Netscape Navigator, Netscape ONE, and the Netscape N and
  Ship's Wheel logos are registered trademarks of Netscape Communications
  Corporation in the United States and other countries.
  
 */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <dirent.h>
#include <unistd.h>  /* uid_t */
#include <time.h>

#include <glib/gi18n.h>
#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>  /* contains all keyboard defs */
#include "proctool.h"
#include "getline.h"
#include "cookie.xpm"

#define COOKIE_COLUMNS 8

/*
  "The static declaration, applied to an external variable or function,
  limits the scope of that object to the rest of the source file being
  compiled."  -- K & R, p. 83
*/

int intcmp(const char *s1, const char *s2) {
    double v1, v2;

    v1 = atof(s1);
    v2 = atof(s2);

    if (v1 < v2) {
	return -1;
    } else if (v1 > v2) {
	return 1;
    } else {
	return 0;
    }
}

/* COOKIE FUNCTIONS
   A cookie is simply an array of pointers to gchar; each pointer to
   gchar holds the string value of one of the cookie fields:
    "Domain", - domain of the server that set the cookie
    "Suffix", - T/F: is the domain a suffix, or a full domain?
    "Path", - what path does the browser have to be in to return the cookie?
    "Secure", - T/F: is this cookie only sent over secure http?
    "Expire", - time in seconds, since the epoch, this cookie expires
    "Name", - name (or key) of the cookie
    "Value", - value of the cookie
    "Expire". - human-readable expiration date
    Please note that the final "Expire" is not kept in the cookie
    file, but is a human-readable version of the date held in the
    first "Expire".
*/

/* is a line of text from the cookie file a valid cookie? */
static gboolean is_a_cookie_line(gchar *line);

/* split a line of text into the seven cookie fields, and place
   in new_cookie */
static void split_cookie_line(gchar *line, gchar *new_cookie[COOKIE_COLUMNS]);

/* set a cookie's gchar pointers to NULL */
static void initialise_cookie(gchar *new_cookie[COOKIE_COLUMNS]);

/* free the memory pointed to by all the gchar pointers in a cookie,
   and set each gchar pointer to NULL */
static void free_cookie(gchar *c[COOKIE_COLUMNS]);

/* copy the first cookie's strings to the gchar ptrs of the second
   cookie, allocating the needed memory to do so */
static void copy_cookie(gchar *c1[COOKIE_COLUMNS], gchar *c2[COOKIE_COLUMNS]);

/* copy the cookie from a particular row in the clist to c,
   allocating the needed memory to do so */
static void copy_clist_row_to_cookie(gint row, gchar *c[COOKIE_COLUMNS]);

/* handy debugging function to print the contents of a cookie */
#ifdef _MANNI_DEBUG
static void dbg_print_cookie(gchar *c[COOKIE_COLUMNS]) {
    printf("domain: %s\n", c[0]);
    printf("suffix: %s\n", c[1]);
    printf("path: %s\n", c[2]);
    printf("secure: %s\n", c[3]);
    printf("expire: %s\n", c[4]);
    printf("name: %s\n", c[5]);
    printf("value: \"%s\"\n", c[6]);
    printf("expire: %s\n", c[7]);
}
#endif

/* CLIST FUNCTIONS
   Functions used to manipulate the list of cookies held in the
   clist in the main window.
*/

/* parse glbl_cookie_FILE and populate the main clist widget with the
   results */
static void populate_glbl_clist(void);

/* clear all items from main clist */
static void clear_glbl_clist(void);

/* remove a row from main clist */
static void remove_selected_row_from_glbl_clist(GtkWidget *calling_widget, gpointer func_data);

/* enumerate two sorting styles: sorting by string, and sorting
   by integer */
typedef enum { STRING, INTEGER } SORT_STYLE;

/* MANNI: the fact that I had to move the enum up here points to the fact
   that I prabably should move all global var declarations up above
   the global function declarations */

/* sort the clist using a quicksort */
static void quicksort_clist(GtkWidget *clist,
			    SORT_STYLE sort_style,
			    gint sort_column,
			    gint first_index,
			    gint last_index);

/* a utility function to help quicksort_clist work */
static void partition_clist(GtkWidget *clist,
			    SORT_STYLE sort_style,
			    gint sort_column,
			    gint first_index,
			    gint *pivot_index,
			    gint last_index);

/* swap the contents of two rows in the clist */
static void swap_row_clist(GtkWidget *clist, gint row1, gint row2);

/* respond to a double-click in the clist widget (by calling
 show_edit_dialog_box) */
static gboolean select_clist_dblclk(GtkWidget *clist,
			  GdkEventButton *event,
			  gpointer func_data);

/* copy the cookie from the selected row to the global variable which
   holds the currendly-selected cookie, glbl_selected_cookie */
static void select_clist (GtkWidget *clist,
		   gint row, 
		   gint column, 
		   GdkEventButton * bevent);

/* perform a sort on the column if its title button has been clicked */
/* PLEASE NOTE that sort_by_column_glbl_clist does not sort the clist
   arg, but sorts glbl_clist */
static void sort_by_column_glbl_clist (GtkWidget *clist,
			 gint column,
			 GdkEventButton * bevent);

/* inserts a new cookie in clist, and activates the edit dialog
 box for the new cookie.*/
static void insert_row_glbl_clist (GtkWidget *calling_widget, gpointer func_data);

/* creates the actual global clist widget */
static void create_glbl_clist (GtkWidget *box1);

/* copies the contents of glbl_selected_cookie into
   the correct row of the master clist (called by clicking
   "OK" on the cookie edit dialogue box), then destroys
   the edit dialogue box */
static void update_glbl_clist(GtkWidget *calling_widget,
			      GtkWidget *edit_win_to_destroy);

/* handle a keypress in the clist
   If <Del> was pressed, delete the currently selected cookie */
gint handle_keypress_in_main_win(GtkWidget *calling_widget, GdkEventKey *event, gpointer func_data);


/* COOKIE EDIT DIALOGUE BOX FUNCTIONS */

/* pops up the cookie edit dialogue box */
static void show_edit_dialog_box (GtkWidget *calling_widget, gpointer func_data);

/* whenever an editing change is made to any of the fields
 of a cookie, glbl_selected_cookie has memory re-allocated,
and strings re-copied, to reflect the changes made in
the dialogue box */
static void change_callback(GtkWidget *entry, gpointer func_data);

/* detects text changes in the human-readable date field of the
   cookie edit dialogue box, and changes the original expiry date
   field on the fly */
static void change_time_callback(GtkWidget *entry, gpointer func_data);

/* called when one of the true/false checkboxes in the cookie edit
   dialogue box changes state */
static void change_tf_callback(GtkWidget *toggle_button, gpointer func_data);

/* handle a keypress in the edit dialogue.
   If <Return> was pressed, destroy the window and save the cookie
   If <Esc> was pressed, destroy the find window and don't save the cookie. */
gint handle_keypress_in_edit_win(GtkWidget *edit_window, GdkEventKey *event, gpointer func_data);

/* FIND DIALOG BOX FUNCTIONS */

/* show the 'find' dialogue box */
static void show_find_dialog_box(GtkWidget *calling_widget, gpointer func_data);
static void search (GtkWidget*, gpointer);
static gint search_dlg_key_cb (GtkWidget*, GdkEventKey*, gpointer);
static void clear_search (GtkWidget*, gpointer);

/* make glbl_last_found_row equal -1 again */
static void reset_last_found_row(GtkWidget *calling_widget, gpointer func_data);

/* set case sensitive searching TRUE/FALSE */
/* (held in the global variable glbl_search_case_sensitive) */
static void set_tf_case_sensitive_search(GtkWidget *toggle_button, gpointer func_data);

/* OTHER DIALOG BOX FUNCTIONS */

/* show the 'about' dialogue box */
static void show_about_dialog_box(GtkWidget *calling_widget, gpointer func_data);

/* show an error dialogue box, with the following message */
static void show_error_dialog_box (gchar *message);

/* FILE SELECTION AND SAVE FUNCTIONS */

/* respond to the "OK" button in the open file dialogue */
static void file_selection_ok (GtkWidget *w, GtkFileSelection *fs);

/* respond to the "OK" button in the save_as dialogue box */
static void save_as_ok (GtkWidget *w, GtkFileSelection *fs);

/* creates the file selection dialogue box that you see when you use
   "open" */
static void show_open_file_dialog_box (GtkWidget *calling_widget, gpointer func_data);

/* creates the "save as" dialoge box */
static void show_save_as_dialog_box (GtkWidget *calling_widget, gpointer func_data);

/* handles the save_file request from the file manu; is smart enough
   to either call write_file() if the file exists, or call
   show_save_as_dialog_box if glbl_selected_filename_str is empty */
static void save_file(GtkWidget *calling_widget, gpointer func_data);

/* actually writes all of the cookie data held in the main clist
   out into a properly formatted mozilla file whose name
   should be held in glbl_selected_filename_str */
static void write_file(void);

/* OTHER FUNCTIONS */

/* check to see if there is a netscape lockfile for the current user,
   and if so, issue a warning */
static void check_for_running_netscape(void);

/* change the cursor -- seems not to be working */
/* static void set_cursor (guint c); */

/* ALL GLOBAL VARIABLES START WITH glbl_ */

/* cookie menu */
static GtkWidget *glbl_cookie_menu_item_holder;
static GtkWidget *glbl_cookie_menu_items;
static GtkWidget *glbl_cookie_menu;

/* the main programme window */
static GtkWidget *glbl_main_window;

/* title of the main programme window */
static GString *glbl_window_title_gstr;

/* the master combo list */
static GtkWidget *glbl_clist;

/* we need a global pointer to point to the expire time label in
   an edit window whenever the edit window is constructed. This will
   give us a handle to the label even after the function is run. We need
   the handle to the label so that we can change the value of the label
   as handler functions are called */
static GtkWidget *glbl_selected_etl;

/* we need a time struct to throw the results of strptime() into */
static struct tm glbl_selected_time;

/* we also need a time_t global var to convert glbl_selected_time from */
static time_t glbl_selected_time_sec;

/* finally, we need an ascii string representation of the
   glbl_selected_time_sec.
   January 2038 is when a 32 bit int will roll over, and that 32 bit int
   seems to need 10 ascii characters to print. I'll therefore allocate 12
   characters to be safe. This won't, however, be safe if we move to 64 bit
   ints */
static gchar glbl_selected_time_str[12];

static gint glbl_clist_rows = 0;
static gint glbl_clist_selected_row = 0;
static gchar *glbl_selected_cookie[COOKIE_COLUMNS]; /* the currently selected cookie in the glbl_clist */


/* glbl_selected_filename_str will be a string holding the name of the
   file from
   a file select box, or the default cookie file location */
static gchar *glbl_selected_filename_str = NULL;
static gchar *glbl_home_directory_str = NULL; /* user's home directory */
/* static gchar *glbl_netscape_dot_dir_str = NULL; netscape dot directory */
static FILE *glbl_cookie_FILE = NULL;
    
static gchar *glbl_cookie_titles_str[] = {
    "Domain",
    "Suffix",
    "Path",
    "Secure",
    "Expire (sec)",
    "Name",
    "Value",
    "Expire"
};

/* enumerate a label for each column name so that we can use
   a column name instead of a column number. cookie[Domain] tells
   us more than cookie[0] */
enum COLUMN_NAMES {COL_DOMAIN,
		   COL_SUFFIX,
		   COL_PATH,
		   COL_SECURE,
		   COL_EXPIRE_SECONDS,
		   COL_NAME,
		   COL_VALUE,
		   COL_EXPIRE_DATE};

/* sad, but true: a callback function that I attach to each text
   entry field in the cookie popup window requires a pointer to
   an int as an argument, so I have to maintain this array of pointers
   to ints just to keep track of which of the 8 text fields is being
   used. */
static gint *glbl_int_ptr[COOKIE_COLUMNS];

GdkPixmap *glbl_cookie_xpm;
GdkBitmap *glbl_cookie_xpm_mask;

/* keeps track of the row of the last successful find */
static gint glbl_last_found_row = -1;

/* keeps track of the last column that was sorted. If the value
   is -1, no column has been sorted */
static gint glbl_last_sorted_column = -1;

static gboolean glbl_search_case_sensitive = FALSE;

/*
  main
*/

int main (int argc, gchar *argv[]) {

    GtkWidget *file_menu_item_holder;
    GtkWidget *menu_bar;
    GtkWidget *file_menu;
    GtkWidget *menu_items;
    GtkWidget *vbox;

    GtkWidget *help_menu_item_holder;
    GtkWidget *help_menu_items;
    GtkWidget *help_menu;

    GtkWidget *edit_menu_item_holder;
    GtkWidget *edit_menu_items;
    GtkWidget *edit_menu;

    GdkColor *transparent = NULL;

	GtkAccelGroup* accel_group;

    int i;

    gtk_init (&argc, &argv);

    /* initialise the pointers to ints */
    for (i = 0; i < COOKIE_COLUMNS; i++) {
	glbl_int_ptr[i] = malloc(sizeof(int));
	*glbl_int_ptr[i] = i;
    }

    /* initialise the window title */
    glbl_window_title_gstr = g_string_new("gtkcookie");

    /* create a new glbl_main_windoindw */
    glbl_main_window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_widget_set_usize( GTK_WIDGET (glbl_main_window), 700, 300);
    gtk_window_set_title(GTK_WINDOW (glbl_main_window), glbl_window_title_gstr->str);
    gtk_signal_connect(GTK_OBJECT (glbl_main_window), "delete_event",
		       GTK_SIGNAL_FUNC(gtk_main_quit),
		       NULL);
    
    /* handle any keypresses in the clist. For now, we only
       care about Del, but that could change. */
    gtk_signal_connect(GTK_OBJECT(glbl_main_window), "key_press_event",
		       GTK_SIGNAL_FUNC(handle_keypress_in_main_win),
		       NULL);

    /* add the icon to the glbl_main_window */

    gtk_widget_realize(glbl_main_window);
    
    glbl_cookie_xpm = gdk_pixmap_create_from_xpm_d (glbl_main_window->window,
						    &glbl_cookie_xpm_mask, 
						    transparent, 
						    cookiepic);

    gdk_window_set_icon(glbl_main_window->window, NULL,
			glbl_cookie_xpm, glbl_cookie_xpm);

	/* add accelerator_table to the window */
	accel_group = gtk_accel_group_new();
	gtk_window_add_accel_group(GTK_WINDOW(glbl_main_window), accel_group);

    /* FILE MENU */
    
    /* Init the menu-widget, and remember -- never
     * gtk_show_widget() the file_menu_item_holder widget!! 
     * This is the menu that holds the menu items, the one that
     * will pop up when you click on the "File" menu in the app */
    file_menu_item_holder = gtk_menu_new();
    
    /* Next we make the menu-entries for "file_menu_item_holder".
     * Notice the call to gtk_menu_append.  Here we are adding a list of
     * menu items to our menu.  Normally, we'd also catch the "clicked"
     * signal on each of the menu items and setup a callback for it,
     * but it's omitted here to save space. */

    /***************/
    
    /* Create a new menu-item with a name... */
    menu_items = gtk_menu_item_new_with_label("Open...");
	
    /* ...and add it to the file_menu_item_holder. */
    gtk_menu_append(GTK_MENU (file_menu_item_holder), menu_items);
	
    /* Do something interesting when the menuitem is selected */
    gtk_signal_connect_object(GTK_OBJECT(menu_items), "activate",
			      GTK_SIGNAL_FUNC(show_open_file_dialog_box), NULL);
	/* install the accelerator */
	gtk_widget_add_accelerator(menu_items, "activate", accel_group, 'O',
							   GDK_CONTROL_MASK, 0);

    /* Show the widget */
    gtk_widget_show(menu_items);

    /***************/

    /***************/
    
    /* Create a new menu-item with a name... */
    menu_items = gtk_menu_item_new_with_label("Save");
	
    /* ...and add it to the file_menu_item_holder. */
    gtk_menu_append(GTK_MENU (file_menu_item_holder), menu_items);
	
    /* Do something interesting when the menuitem is selected */
    gtk_signal_connect_object(GTK_OBJECT(menu_items), "activate",
			      GTK_SIGNAL_FUNC(save_file), NULL);
    
	/* install the accelerator */
	gtk_widget_add_accelerator(menu_items, "activate", accel_group, 'S',
							   GDK_CONTROL_MASK, 0);
	
    /* Show the widget */
    gtk_widget_show(menu_items);

    /***************/

    /***************/
    
    /* Create a new menu-item with a name... */
    menu_items = gtk_menu_item_new_with_label("Save as...");
	
    /* ...and add it to the file_menu_item_holder. */
    gtk_menu_append(GTK_MENU (file_menu_item_holder), menu_items);
	
    /* Do something interesting when the menuitem is selected */
    gtk_signal_connect_object(GTK_OBJECT(menu_items), "activate",
			      GTK_SIGNAL_FUNC(show_save_as_dialog_box), NULL);
	
    /* Show the widget */
    gtk_widget_show(menu_items);

    /***************/

    /***************/
    
    /* Create a new blank menu-item (which creates a line ... */
    menu_items = gtk_menu_item_new();
	
    /* ...and add it to the file_menu_item_holder. */
    gtk_menu_append(GTK_MENU(file_menu_item_holder), menu_items);

    /* no need to attach a function to a separator... */
	
    /* Show the widget */
    gtk_widget_show(menu_items);

    /***************/

    /***************/
    
    /* Create a new menu-item with a name... */
    menu_items = gtk_menu_item_new_with_label("Quit");
	
    /* ...and add it to the file_menu_item_holder. */
    gtk_menu_append(GTK_MENU (file_menu_item_holder), menu_items);
	
    /* quit when the "Exit" menuitem is selected */
    gtk_signal_connect(GTK_OBJECT (menu_items), "activate",
		       GTK_SIGNAL_FUNC(gtk_main_quit),
		       NULL);

	/* install the accelerator */
	gtk_widget_add_accelerator(menu_items, "activate", accel_group, 'Q',
							   GDK_CONTROL_MASK, 0);

    /* Show the widget */
    gtk_widget_show(menu_items);

    /***************/
    
    /* This is the file menu, and will be the label
     * displayed on the menu bar.  There won't be a signal handler attached,
     * as it only pops up the rest of the menu when pressed. */
    file_menu = gtk_menu_item_new_with_label("File");
    
    gtk_widget_show(file_menu);
    
    /* Now we specify that we want our newly created "file_menu_item_holder" to be the menu
     * for the "file menu" */
    gtk_menu_item_set_submenu(GTK_MENU_ITEM (file_menu), file_menu_item_holder);

    
    /* COOKIE MENU */
    
    glbl_cookie_menu_item_holder = gtk_menu_new();

    /********/
    glbl_cookie_menu_items = gtk_menu_item_new_with_label("New...");
	
    gtk_menu_append(GTK_MENU (glbl_cookie_menu_item_holder),
		    glbl_cookie_menu_items);
	
    gtk_signal_connect_object(GTK_OBJECT(glbl_cookie_menu_items), "activate",
			      GTK_SIGNAL_FUNC(insert_row_glbl_clist),
			      NULL);
	/* install the accelerator */
	gtk_widget_add_accelerator(glbl_cookie_menu_items, "activate", accel_group, 'N',
							   GDK_CONTROL_MASK, 0);

    gtk_widget_show(glbl_cookie_menu_items);
    /*********/

    /********/
    glbl_cookie_menu_items = gtk_menu_item_new_with_label("Edit...");
	
    gtk_menu_append(GTK_MENU (glbl_cookie_menu_item_holder),
		    glbl_cookie_menu_items);
	
    gtk_signal_connect_object(GTK_OBJECT(glbl_cookie_menu_items), "activate",
			      GTK_SIGNAL_FUNC(show_edit_dialog_box),
			      NULL);
	/* install the accelerator */
	gtk_widget_add_accelerator(glbl_cookie_menu_items, "activate", accel_group, 'E',
							   GDK_CONTROL_MASK, 0);

    gtk_widget_show(glbl_cookie_menu_items);
    /*********/

    /********/
    glbl_cookie_menu_items = gtk_menu_item_new_with_label("Delete");
	
    gtk_menu_append(GTK_MENU (glbl_cookie_menu_item_holder),
		    glbl_cookie_menu_items);
	
    gtk_signal_connect_object(GTK_OBJECT(glbl_cookie_menu_items), "activate",
			      GTK_SIGNAL_FUNC(remove_selected_row_from_glbl_clist),
			      NULL);
    /* add "Del" to this one. It won't work as a keyboard accelerator,
       so find another way */
	
    gtk_widget_show(glbl_cookie_menu_items);
    /*********/

    glbl_cookie_menu = gtk_menu_item_new_with_label("Cookie");

    gtk_widget_show(glbl_cookie_menu);
    
    gtk_menu_item_set_submenu(GTK_MENU_ITEM (glbl_cookie_menu),
			      glbl_cookie_menu_item_holder);

    /* HELP MENU */
    
    /* Init the menu-widget, and remember -- never
     * gtk_show_widget() the file_menu_item_holder widget!! 
     * This is the menu that holds the menu items, the one that
     * will pop up when you click on the "File" menu in the app */
    help_menu_item_holder = gtk_menu_new();
    
    /* Next we make the menu-entries for "file_menu_item_holder".
     * Notice the call to gtk_menu_append.  Here we are adding a list of
     * menu items to our menu.  Normally, we'd also catch the "clicked"
     * signal on each of the menu items and setup a callback for it,
     * but it's omitted here to save space. */

    /***************/
    
    /* Create a new menu-item with a name... */
    help_menu_items = gtk_menu_item_new_with_label("About...");
	
    /* ...and add it to the file_menu_item_holder. */
    gtk_menu_append(GTK_MENU (help_menu_item_holder), help_menu_items);
	
    /* Do something interesting when the menuitem is selected */
    gtk_signal_connect_object(GTK_OBJECT(help_menu_items), "activate",
			      GTK_SIGNAL_FUNC(show_about_dialog_box), NULL);
	
    /* Show the widget */
    gtk_widget_show(help_menu_items);

    /***************/

    /* This is the file menu, and will be the label
     * displayed on the menu bar.  There won't be a signal handler attached,
     * as it only pops up the rest of the menu when pressed. */
    help_menu = gtk_menu_item_new_with_label("Help");
    
    gtk_widget_show(help_menu);
    
    /* Now we specify that we want our newly created "file_menu_item_holder" to be the menu
     * for the "file menu" */
    gtk_menu_item_set_submenu(GTK_MENU_ITEM (help_menu), help_menu_item_holder);


    /* /HELP MENU */

    /* EDIT MENU */
    
    /* Init the menu-widget, and remember -- never
     * gtk_show_widget() the file_menu_item_holder widget!! 
     * This is the menu that holds the menu items, the one that
     * will pop up when you click on the "File" menu in the app */
    edit_menu_item_holder = gtk_menu_new();
    
    /* Next we make the menu-entries for "file_menu_item_holder".
     * Notice the call to gtk_menu_append.  Here we are adding a list of
     * menu items to our menu.  Normally, we'd also catch the "clicked"
     * signal on each of the menu items and setup a callback for it,
     * but it's omitted here to save space. */

    /***************/
    
    /* Create a new menu-item with a name... */
    edit_menu_items = gtk_menu_item_new_with_label("Find...");
	
    /* ...and add it to the file_menu_item_holder. */
    gtk_menu_append(GTK_MENU (edit_menu_item_holder), edit_menu_items);
	
    /* Do something interesting when the menuitem is selected */
    gtk_signal_connect_object(GTK_OBJECT(edit_menu_items), "activate",
			      GTK_SIGNAL_FUNC(show_find_dialog_box),
			      NULL);
    
	/* install the accelerator */
	gtk_widget_add_accelerator(edit_menu_items, "activate", accel_group, 'F',
							   GDK_CONTROL_MASK, 0);

    /* Show the widget */
    gtk_widget_show(edit_menu_items);

    /***************/

    /* This is the file menu, and will be the label
     * displayed on the menu bar.  There won't be a signal handler attached,
     * as it only pops up the rest of the menu when pressed. */
    edit_menu = gtk_menu_item_new_with_label("Edit");
    
    gtk_widget_show(edit_menu);
    
    /* Now we specify that we want our newly created "file_menu_item_holder" to be the menu
     * for the "file menu" */
    gtk_menu_item_set_submenu(GTK_MENU_ITEM (edit_menu), edit_menu_item_holder);


    /* /EDIT MENU */
    
    /* VBOX */
    
    /* A vbox to put a menu and a button in: */
    vbox = gtk_vbox_new(FALSE, 0);
    gtk_container_add(GTK_CONTAINER(glbl_main_window), vbox);
    gtk_widget_show(vbox);
    
    /* Create a menu-bar to hold the menus and add it to our main glbl_main_window */
    menu_bar = gtk_menu_bar_new();
    gtk_box_pack_start(GTK_BOX(vbox), menu_bar, FALSE, FALSE, 2);
    gtk_widget_show(menu_bar);
    
    /* embed the glbl_clist here */
    create_glbl_clist(vbox);

    /* And finally we append the menu-item to the menu-bar -- this is the
     * "File" menu-item I have been raving about =) */
    gtk_menu_bar_append(GTK_MENU_BAR (menu_bar), file_menu);
    gtk_menu_bar_append(GTK_MENU_BAR (menu_bar), edit_menu);
    gtk_menu_bar_append(GTK_MENU_BAR (menu_bar), glbl_cookie_menu);
    gtk_menu_bar_append(GTK_MENU_BAR (menu_bar), help_menu);

    /* always display the glbl_main_window as the last step
       so it all splashes on the screen at once. */
    gtk_widget_show(glbl_main_window);

    /* fill the glbl_clist widget with all of the cookies */
    populate_glbl_clist();

    /* after populating the clist, see if there is a netscape
       lock file, and be good enough to let the user know about it */
    check_for_running_netscape();

    gtk_main ();

    /* free the memory from the pointers to ints */
    for (i = 0; i < COOKIE_COLUMNS; i++) {
	free(glbl_int_ptr[i]);
    }


    return 0;
}


static void save_file(GtkWidget *calling_widget, gpointer func_data) {
    if (glbl_selected_filename_str == NULL) {
	/* show_save_as_dialog_box, when called as a callback,
	   takes a pointer to the calling widget, and a pointer
	   to extra function data, if required */
	show_save_as_dialog_box(NULL, NULL);
    } else {
	write_file();
    }

}

static void write_file (void) {
    GString *error_msg_gstr;
    gchar *cell_contents;
    int i;
    int row;

    /* set the title of the main window to show the selected filename */
    /* first, erase what's currently in the gstring */
    g_string_erase(glbl_window_title_gstr, 0, glbl_window_title_gstr->len);
    /* now create the new title, always starting with "gtkcookie - " and
       then the filename */
    g_string_assign(glbl_window_title_gstr, "gtkcookie - ");
    g_string_append(glbl_window_title_gstr, glbl_selected_filename_str);
    /* set the title of the main window */
    gtk_window_set_title(GTK_WINDOW (glbl_main_window), glbl_window_title_gstr->str);
    
    glbl_cookie_FILE = fopen(glbl_selected_filename_str, "w");
    if (glbl_cookie_FILE == NULL) {
	error_msg_gstr = g_string_new("");
	g_string_sprintf(error_msg_gstr, "Can't write the cookie file\n\"%s\".\n(Probably not allowed to write\nto this directory, or\nthe filesystem is full.)", glbl_selected_filename_str);
	show_error_dialog_box(error_msg_gstr->str);
	/* don't forget to deallocate the string after we're done
	   with it! Fortunately, deallocating the string seems not
	   to harm the error message created by show_error_dialog_box() */
	g_string_free(error_msg_gstr, TRUE);
	return;
    }

    fprintf(glbl_cookie_FILE, "# Netscape HTTP Cookie File\n");
    fprintf(glbl_cookie_FILE, "# http://www.netscape.com/newsref/std/cookie_spec.html\n");
    fprintf(glbl_cookie_FILE, "# This is a generated file!  Do not edit.\n\n");

    for (row = 0; row < glbl_clist_rows; row++) {
	for (i = 0; i < (COOKIE_COLUMNS - 1); i++) {
	    gtk_clist_get_text(GTK_CLIST (glbl_clist), row, i, &cell_contents);
	    if (i < (COOKIE_COLUMNS - 2)) {
		fprintf(glbl_cookie_FILE, "%s\t", cell_contents);
	    } else {
		fprintf(glbl_cookie_FILE, "%s\n", cell_contents);
	    }
	}
    }

    fclose(glbl_cookie_FILE);
}

static void show_about_dialog_box(GtkWidget *calling_widget, gpointer func_data) {
    static GtkWidget *about_window = NULL;
    GtkWidget *box1;
    GtkWidget *about_text_label;

	if (!about_window) {
		GtkWidget *btn;

		about_window = gtk_dialog_new();
		gtk_dialog_set_has_separator (GTK_DIALOG (about_window), FALSE);
	gtk_window_position(GTK_WINDOW(about_window), GTK_WIN_POS_MOUSE);

	gtk_signal_connect (GTK_OBJECT(about_window), "destroy",
			    GTK_SIGNAL_FUNC(gtk_widget_destroyed),
			    &about_window);

	gtk_window_set_title (GTK_WINDOW (about_window), "About gtkcookie");
	gtk_container_border_width (GTK_CONTAINER (about_window), 0);

		box1 = gtk_vbox_new (FALSE, 10);
		gtk_container_set_border_width (GTK_CONTAINER (box1), 10);
	gtk_container_add (GTK_CONTAINER (GTK_DIALOG (about_window)->vbox), box1);
	gtk_widget_show (box1);

	about_text_label = gtk_label_new("gtkcookie, version 0.03\n\nCode copyright (c) 1998 Manni Wood.\nCookie icons copyright (c) 1998 Glenn Matheson.\n\ngtkcookie comes with ABSOLUTELY NO WARRANTY.\nThis is free software, and you are welcome\nto redistribute it under certain conditions;\nfor details, see the file named 'COPYING' that\ncame with the source code of this programme.\n\nNetscape is a registered trademark of\nNetscape Communications Corporation\nin the United States and other countries.");
		gtk_box_pack_start (GTK_BOX (box1), about_text_label, TRUE, TRUE, 0);
	gtk_widget_show (about_text_label);

		btn = gtk_dialog_add_button (GTK_DIALOG (about_window), GTK_STOCK_CLOSE, GTK_RESPONSE_CLOSE);
		g_signal_connect_swapped (G_OBJECT (btn), "clicked", G_CALLBACK (gtk_widget_destroy), G_OBJECT (about_window));
		gtk_widget_grab_default (btn);
		gtk_widget_show (btn);
	}

    if (!GTK_WIDGET_VISIBLE (about_window)) {
	gtk_widget_show (about_window);
    } else {
	gtk_widget_destroy (about_window);
    }
}

static void
show_find_dialog_box (GtkWidget* wdg, gpointer data) {
	static GtkWidget *search_dlg = NULL;
    GtkWidget *box1;
    GtkWidget *hbox2;
    GtkWidget *hbox1;

    GtkWidget *find_text_label;

    GtkWidget *case_sensitive_tf_checkbox;
    
    /* each time the find dialog box is opened, set glbl_last_found_row
       to zero, to indicate that whatever the user is about to search on,
       it is the first time he will be searching for that string */
    glbl_last_found_row = -1;

	if (!search_dlg) {
		GtkWidget *btn, *needle;

		search_dlg = gtk_dialog_new();
		gtk_window_position (GTK_WINDOW (search_dlg), GTK_WIN_POS_MOUSE);

		g_signal_connect (G_OBJECT (search_dlg), "destroy", G_CALLBACK (gtk_widget_destroyed), &search_dlg);
	/* REALLY IMPORTANT NOTE! gtk_widget_destroyed (notice the
	   past tense) seems to do something a whole lot different
	   than gtk_widget_destroy (notice the present tense)
	*/

		gtk_window_set_title (GTK_WINDOW (search_dlg), _("Find cookie"));

	box1 = gtk_vbox_new (FALSE, 0);
		gtk_container_add (GTK_CONTAINER (GTK_DIALOG (search_dlg)->vbox), box1);
	gtk_widget_show (box1);

	hbox1 = gtk_hbox_new (FALSE, 10);
	gtk_container_border_width (GTK_CONTAINER (hbox1), 10);
	gtk_box_pack_start (GTK_BOX (box1), hbox1, TRUE, TRUE, 0);
	gtk_widget_show (hbox1);


	find_text_label = gtk_label_new("Find:");
	gtk_box_pack_start (GTK_BOX (hbox1), find_text_label, TRUE, TRUE, 0);
	gtk_widget_show (find_text_label);

		needle = gtk_entry_new();
		g_signal_connect (G_OBJECT (needle), "changed", G_CALLBACK (reset_last_found_row), NULL);
		gtk_box_pack_start (GTK_BOX (hbox1), needle, TRUE, TRUE, 0);
		gtk_widget_grab_focus (needle);
		gtk_widget_show (needle);

	hbox2 = gtk_hbox_new (FALSE, 10);
	gtk_container_border_width (GTK_CONTAINER (hbox2), 10);
	gtk_box_pack_start (GTK_BOX (box1), hbox2, FALSE, TRUE, 0);
	gtk_widget_show (hbox2);


	case_sensitive_tf_checkbox = gtk_check_button_new_with_label ("Case sensitive");
 
	gtk_signal_connect(GTK_OBJECT(case_sensitive_tf_checkbox), "clicked",
			   GTK_SIGNAL_FUNC(set_tf_case_sensitive_search),
			   NULL);

	/* turn the button on or off depending on whether the
	   glbl_search_case_sensitive is "TRUE" or "FALSE" */
	if (glbl_search_case_sensitive == TRUE) {
	    /* checkbox is checked */
	    gtk_toggle_button_set_state( GTK_TOGGLE_BUTTON(case_sensitive_tf_checkbox),
					 TRUE );    
	} else {
	    /* checkbox is unchecked */
	    gtk_toggle_button_set_state( GTK_TOGGLE_BUTTON(case_sensitive_tf_checkbox),
					 FALSE );    
	}

	gtk_box_pack_start (GTK_BOX (hbox2), case_sensitive_tf_checkbox, FALSE, TRUE, 0);
	gtk_widget_show (case_sensitive_tf_checkbox);
	/********************/

		btn = gtk_dialog_add_button (GTK_DIALOG (search_dlg), GTK_STOCK_FIND, GTK_RESPONSE_ACCEPT);
		g_signal_connect (G_OBJECT (btn), "clicked", G_CALLBACK (search), needle);
		gtk_widget_show (btn);

		btn = gtk_dialog_add_button (GTK_DIALOG (search_dlg), GTK_STOCK_CLEAR, GTK_RESPONSE_REJECT);
		g_signal_connect (G_OBJECT (btn), "clicked", G_CALLBACK (clear_search), needle);
		gtk_widget_show (btn);

		btn = gtk_dialog_add_button (GTK_DIALOG (search_dlg), GTK_STOCK_CLOSE, GTK_RESPONSE_CLOSE);
		g_signal_connect_swapped (G_OBJECT (btn), "clicked", G_CALLBACK (gtk_widget_destroy), G_OBJECT (search_dlg));
		gtk_widget_show (btn);

		/* check if the user hit Return and if so, call search() */
		g_signal_connect (G_OBJECT (search_dlg), "key_press_event", G_CALLBACK (search_dlg_key_cb), needle);
	}

	if (!GTK_WIDGET_VISIBLE (search_dlg)) {
		gtk_widget_show (search_dlg);
    } else {
		gtk_widget_destroy (search_dlg);
    }
}

static void show_error_dialog_box (gchar *message) {
    static GtkWidget *error_window = NULL;
    GtkWidget *box1;
    GtkWidget *about_text_label;

	if (!error_window) {
		GtkWidget *btn;

		error_window = gtk_dialog_new();
		gtk_dialog_set_has_separator (GTK_DIALOG (error_window), FALSE);
	gtk_window_position(GTK_WINDOW(error_window), GTK_WIN_POS_MOUSE);

	gtk_signal_connect (GTK_OBJECT (error_window), "destroy",
			    GTK_SIGNAL_FUNC(gtk_widget_destroyed),
			    &error_window);

	gtk_window_set_title (GTK_WINDOW (error_window), "Error");
	gtk_container_border_width (GTK_CONTAINER (error_window), 0);

		box1 = gtk_vbox_new (FALSE, 10);
		gtk_container_border_width (GTK_CONTAINER (box1), 10);
		gtk_container_add (GTK_CONTAINER (GTK_DIALOG (error_window)->vbox), box1);
	gtk_widget_show (box1);

	about_text_label = gtk_label_new(message);
		gtk_box_pack_start (GTK_BOX (box1), about_text_label, TRUE, TRUE, 0);
	gtk_widget_show (about_text_label);

		btn = gtk_dialog_add_button (GTK_DIALOG (error_window), GTK_STOCK_CLOSE, GTK_RESPONSE_CLOSE);
		g_signal_connect_swapped (G_OBJECT (btn), "clicked", G_CALLBACK (gtk_widget_destroy), G_OBJECT (error_window));
		gtk_widget_grab_default (btn);
		gtk_widget_show (btn);
	}

    if (!GTK_WIDGET_VISIBLE (error_window)) {
	/* gtk_widget_realize (error_window); */
	gtk_grab_add (error_window);
	gtk_widget_show (error_window);
	/* gdk_window_raise (error_window->window); */
    } else {
	gtk_grab_remove (error_window);
	gtk_widget_destroy (error_window);
    }
}


static void clear_glbl_clist(void) {
    gtk_clist_clear (GTK_CLIST (glbl_clist));
    glbl_clist_rows = 0;
}

static void remove_selected_row_from_glbl_clist(GtkWidget *calling_widget, gpointer func_data) {
    gtk_clist_remove (GTK_CLIST (glbl_clist), glbl_clist_selected_row);
    glbl_clist_rows--;
}

static void sort_by_column_glbl_clist (GtkWidget *clist,
			 gint sort_column, 
			 GdkEventButton * bevent) {

    /* if the sort_column is the human readable date,
       make sort_comumn equal the numeric date column,
       because that's what we really want to sort the date on
       anyway. */
    if (sort_column == COL_EXPIRE_DATE) {
	sort_column = COL_EXPIRE_SECONDS;
    }
    
    /* as a small but useful optimisation, let us check to see
       if the sort_column we are about to sort on has already been
       sorted. If so, we can return right now. */
    if (sort_column == glbl_last_sorted_column) {
	return;
    }

    /* find a way to make the icon a watch */
    /* set_cursor(150); seems not to be working */
    /* I freeze graphical updates to the clist while the sort takes place */

    gtk_clist_freeze(GTK_CLIST (glbl_clist));

    if (sort_column == COL_EXPIRE_SECONDS) {
	quicksort_clist(glbl_clist, INTEGER, sort_column, 0, (glbl_clist_rows - 1));
    } else {
	quicksort_clist(glbl_clist, STRING, sort_column, 0, (glbl_clist_rows - 1));
    }

    gtk_clist_thaw(GTK_CLIST (glbl_clist));
    /* find a way to turn the icon from a watch to a pointer */
    /* set_cursor(132); seems not to be working */
    glbl_last_sorted_column = sort_column;

    /* finally, make sure the selected and highlighted cookie
       is the first one! */
    select_clist(glbl_clist, 0, 0, NULL);
}

static void swap_row_clist(GtkWidget *clist, gint row1, gint row2) {
    gchar *tmp_cookie[COOKIE_COLUMNS];
    gchar *cell_contents;
    gint col;

    initialise_cookie(tmp_cookie);  /* always do this with a new cookie! */

    /* if row1 and row2 are the same, then there is no work to be done */
    if (row1 == row2) {
	return;
    }

    copy_clist_row_to_cookie(row1, tmp_cookie);

    for (col = 0; col < COOKIE_COLUMNS; col++) {
	gtk_clist_get_text(GTK_CLIST (clist), row2, col,
			   &cell_contents);
	gtk_clist_set_text(GTK_CLIST (clist), row1, col,
			   cell_contents);
    }
    for (col = 0; col < COOKIE_COLUMNS; col++) {
	gtk_clist_set_text(GTK_CLIST (clist), row2, col,
			   tmp_cookie[col]);
    }
    
    /* free the memory used by the cookie before leaving subroutine! */
    free_cookie(tmp_cookie);
}

static gboolean select_clist_dblclk(GtkWidget *clist,
		  GdkEventButton *event,
		  gpointer func_data) {

    gint row;

    row = glbl_clist_selected_row;
    
    if (GTK_IS_CLIST(clist) && event->type == GDK_2BUTTON_PRESS) {
	show_edit_dialog_box(clist, func_data);
    }

    return FALSE;
}

static void select_clist (GtkWidget *clist,
	 	   gint row, 
		   gint column, 
		   GdkEventButton * bevent) {
    
    gchar *tmp_cookie[COOKIE_COLUMNS];
    gint i;

    /* initialise all of glbl_selected_cookie's gchar pointers to NULL */
    initialise_cookie(tmp_cookie);
    
    for (i = 0; i < COOKIE_COLUMNS; i++) {
	switch (gtk_clist_get_cell_type (GTK_CLIST (clist), row, i)) {
	case GTK_CELL_TEXT:
	    gtk_clist_get_text(GTK_CLIST (clist), row, i, &tmp_cookie[i]);
	    break;

	default:
	    break;
	}
    }

    /* tmp_cookie contains pointers to the strings in the clist,
       but I need a copy of the cookie whose contents I can modify
       without messing with the text in the glbl_clist. Hence, I copy
       tmp_cookie's strings to the global var, glbl_selected_cookie */
    copy_cookie(tmp_cookie, glbl_selected_cookie);

    /* IMPORTANT! set the selected row to the selected row */
    glbl_clist_selected_row = row;

    /* if right-clicked (ie, button 3), pop up the cookie menu */
    /* note that I'm checking if bevent is NULL, because sometimes
       I call this function directly (instead if it being called by
       an event) and therefore just make the last argument (bevent) NULL.
       Checking bevent->button on NULL crashes the programme. */
    if (bevent != NULL && bevent->button == 3) {
	gtk_menu_popup (GTK_MENU(glbl_cookie_menu_item_holder),
			NULL, NULL, NULL, NULL,
			bevent->button, bevent->time);
    }

}


static void insert_row_glbl_clist (GtkWidget *calling_widget, gpointer func_data) {
    /* future:
       1. clear the currently selected cookie
       2. gtk_clist_insert the currently selected cookie
       3. make the just-inserted cookie the selected cookie in the
       glbl_clist (ensure all of the global "this is the selected cookie row"
       variables reflect this change)
       4. scroll the glbl_clist so that the new, selected cookie is visible
       5. call the double-click function that brings up the cookie edit
       window */
    static gchar *text[] = {
	"url.com",
	"TRUE",
	"/",
	"FALSE",
	"0",
	"key",
	"value",
	"Thu Jan  1 00:00:00 1970"
    };

    gtk_clist_insert (GTK_CLIST(glbl_clist), glbl_clist_selected_row, text);
    glbl_clist_rows++;
    glbl_last_sorted_column = -1;  /* because we just added
				      a new row, no columns
				      can be assumed to be
				      sorted */
    gtk_clist_select_row(GTK_CLIST(glbl_clist),
                                 glbl_clist_selected_row,
                                 0 );
    
    show_edit_dialog_box(calling_widget, func_data);
}


static void create_glbl_clist (GtkWidget *box1) {
	/* ScrolledWindow to be put around glbl_clist */
	GtkWidget* sw_clist;

    /* create GtkCList here so we have a pointer to throw at the 
     * button callbacks -- more is done with it later */
    glbl_clist = gtk_clist_new_with_titles (COOKIE_COLUMNS, glbl_cookie_titles_str);

    /* 
     * the rest of the glbl_clist configuration
     */
    gtk_clist_set_row_height (GTK_CLIST (glbl_clist), 20);
    /* 20 pixels high for a single row? */


    /* if the user clicks on a column title button... */
    gtk_signal_connect (GTK_OBJECT (glbl_clist),
			"click_column",
			GTK_SIGNAL_FUNC(sort_by_column_glbl_clist),
			NULL);
    
    /* if the user clicks on a row... */  
    gtk_signal_connect (GTK_OBJECT (glbl_clist), 
			"select_row",
			GTK_SIGNAL_FUNC(select_clist), 
			NULL);


    /* if the user double-clicks on a row... */  
    gtk_signal_connect(GTK_OBJECT(glbl_clist),
		       "button_press_event",
		       GTK_SIGNAL_FUNC(select_clist_dblclk),
		       NULL);


    /* set the width and justification of each cookie column */
    gtk_clist_set_column_width(GTK_CLIST(glbl_clist), COL_DOMAIN, 100);
    gtk_clist_set_column_width(GTK_CLIST(glbl_clist), COL_SUFFIX, 50);
    gtk_clist_set_column_width(GTK_CLIST(glbl_clist), COL_PATH, 50);
    gtk_clist_set_column_width(GTK_CLIST(glbl_clist), COL_SECURE, 50);
    gtk_clist_set_column_width(GTK_CLIST(glbl_clist), COL_EXPIRE_SECONDS, 80);
    gtk_clist_set_column_justification(GTK_CLIST(glbl_clist), COL_EXPIRE_SECONDS, GTK_JUSTIFY_RIGHT);
    gtk_clist_set_column_width(GTK_CLIST(glbl_clist), COL_NAME, 80);
    gtk_clist_set_column_width(GTK_CLIST(glbl_clist), COL_VALUE, 80);
    gtk_clist_set_column_width(GTK_CLIST(glbl_clist), COL_EXPIRE_DATE, 150);
    /* WIDTH HEIGHT */
    /* gtk_widget_set_usize(glbl_clist, width, height); */
    /* but first check to see if there is a window width/height */

    /* what is a selection mode? */
    gtk_clist_set_selection_mode (GTK_CLIST (glbl_clist), GTK_SELECTION_BROWSE);
	/* what is a policy? */
	sw_clist = gtk_scrolled_window_new(NULL, NULL);
	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(sw_clist), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
	gtk_container_add(GTK_CONTAINER(sw_clist), glbl_clist);

	gtk_container_border_width (GTK_CONTAINER (glbl_clist), 0);
	gtk_box_pack_start (GTK_BOX (box1), sw_clist, TRUE, TRUE, 0);
	gtk_widget_show (glbl_clist);
	gtk_widget_show(sw_clist);
}

/* POPULATE GLBL_CLIST */

static void populate_glbl_clist(void) {
    gchar *file_line = NULL;

    /* were we successful reading a line from the file? */
    gboolean success = FALSE;
    
    gchar *test_cookie[COOKIE_COLUMNS];
    GString *error_msg_gstr;
    size_t pathname_str_length;

    glbl_clist_rows = 0;  /* seeing as we are starting fresh, the
			total number of rows in the glbl_clist widget is 0 */

    glbl_last_sorted_column = -1;  /* also, because we
				      are starting fresh,
				      the last sorted column
				      becomes unknown (-1)
				      again */

    /* set all gchar pointers in test_cookie to zero */
    initialise_cookie(test_cookie);

    /* if the selected filename is NULL, then we look
       in the default location for the cookie file */
    if (glbl_selected_filename_str == NULL) {
	glbl_home_directory_str = getenv("HOME");
	if (glbl_home_directory_str == NULL) {
	    show_error_dialog_box ("Environmental variable HOME not set;\ntherefore, can't find home directory\nor .netscape directory.");
	    return;
	}
	pathname_str_length = strlen(glbl_home_directory_str) + strlen("/.netscape/cookies");
	glbl_selected_filename_str = calloc(pathname_str_length + 1, sizeof(gchar));
	strncpy(glbl_selected_filename_str, glbl_home_directory_str, strlen(glbl_home_directory_str));
	strcat(glbl_selected_filename_str, "/.netscape/cookies");
	glbl_selected_filename_str[pathname_str_length] = '\0';
    }

    /* set the title of the main window to show the selected filename */
    /* first, erase what's currently in the gstring */
    g_string_erase(glbl_window_title_gstr, 0, glbl_window_title_gstr->len);
    /* now create the new title, always starting with "gtkcookie - " and
       then the filename */
    g_string_assign(glbl_window_title_gstr, "gtkcookie - ");
    g_string_append(glbl_window_title_gstr, glbl_selected_filename_str);
    /* set the title of the main window */
    gtk_window_set_title(GTK_WINDOW (glbl_main_window), glbl_window_title_gstr->str);

    glbl_cookie_FILE = fopen(glbl_selected_filename_str, "r");
    if (glbl_cookie_FILE == NULL) {
	/* if we couldn't open the cookie file, set
	   glbl_selected_filename_str to
	   NULL so that if the user says 'save', a 'save as' will be
	   invoked instead. */
	if (glbl_selected_filename_str != NULL) {
	    free(glbl_selected_filename_str);
	    glbl_selected_filename_str = NULL;
	}
	error_msg_gstr = g_string_new("");
	g_string_sprintf(error_msg_gstr, "can't open the cookie file\n\"%s\"\nfor reading.", glbl_selected_filename_str);
	show_error_dialog_box(error_msg_gstr->str);
	/* DEALLOCATE! */
	g_string_free(error_msg_gstr, TRUE);
	return;
    }

    file_line = get_line_safely(glbl_cookie_FILE, &success);
    if (success && is_a_cookie_line(file_line)) {
	split_cookie_line(file_line, test_cookie);
	gtk_clist_append(GTK_CLIST(glbl_clist), test_cookie);
	glbl_clist_rows++;  /* do this whenever you gtk_clist_append() */
	free_cookie(test_cookie);
    }
    free(file_line);
    file_line = NULL;

    while (success) {
	file_line = get_line_safely(glbl_cookie_FILE, &success);
	if (success && is_a_cookie_line(file_line)) {
	    split_cookie_line(file_line, test_cookie);
	    gtk_clist_append(GTK_CLIST(glbl_clist), test_cookie);
	    glbl_clist_rows++;  /* do this whenever you gtk_clist_append() */
	    free_cookie(test_cookie);
	}
	free(file_line);
	file_line = NULL;
    }

    fclose(glbl_cookie_FILE);
}

/* COOKIE FUNCTIONS */

static gboolean is_a_cookie_line(gchar *line) {
    /* a valid cookie line has 6 tabs */
    int tab_count;
    size_t i;

    /* always deal with the possibility of a null string! */
    if (line == NULL) {
	return FALSE;
    }
    
    tab_count = 0;
    for (i = 0; i < strlen(line); i++) {
	if (line[i] == '\t') {
	    tab_count++;
	}
    }

    if (tab_count == 6) {
	return TRUE;
    } else {
	return FALSE;
    }
}

/* make sure each pointer to gchar in new_cookie points to
   zero. This will make free_cookie() work better, because
   it will only free non-zero pointers */
static void initialise_cookie(gchar *new_cookie[COOKIE_COLUMNS]) {
    int i;
    for (i = 0; i < COOKIE_COLUMNS; i++) {
	new_cookie[i] = NULL;
    }
}

static void split_cookie_line(gchar *line, gchar *new_cookie[COOKIE_COLUMNS]) {
    /* from the string 'line', all the following ints
       will hold the start position and length of each cookie part
    */
    gchar *line_ptr;  /* pointer that will be incremented along a string */
    size_t domain_start, domain_length;
    size_t suffix_start, suffix_length;
    size_t path_start, path_length;
    size_t secure_start, secure_length;
    size_t expire_start, expire_length;
    size_t name_start, name_length;
    size_t value_start, value_length;

    size_t tab_locations[6];
    size_t number_of_tabs_found;

    size_t line_length; /* length of 'line' not including terminating '\0' */
    size_t i;  /* iterator */

    time_t cookie_time;
    struct tm *cookie_tm_ptr;

    /* make line_ptr and line equal. Later, line_ptr will get
       incremented along the string that line points to */
    line_ptr = line;

    line_length = strlen(line);

    number_of_tabs_found = 0;

    for (i = 0; i <= line_length; i++) {
	if (line[i] == '\t') {
	    tab_locations[number_of_tabs_found] = i;
	    number_of_tabs_found++;
	    /* DON'T even think about trying to assign an extra tab
	       past the bounds of the array! So if there are too many
	       tabs, break out of the loop! */
	    if (number_of_tabs_found >= 6) {
		break;
	    }
	}
    }

    /* enum COLUMN_NAMES {COL_DOMAIN,
		   COL_SUFFIX,
		   COL_PATH,
		   COL_SECURE,
		   COL_EXPIRE_SECONDS,
		   COL_NAME,
		   COL_VALUE,
		   COL_EXPIRE_DATE};*/

    domain_start = 0;
    suffix_start = tab_locations[0] + 1;
    path_start = tab_locations[1] + 1;
    secure_start = tab_locations[2] + 1;
    expire_start = tab_locations[3] + 1;
    name_start = tab_locations[4] + 1;
    value_start = tab_locations[5] + 1;

    domain_length = suffix_start - domain_start - 1;
    suffix_length = path_start - suffix_start - 1;
    path_length = secure_start - path_start - 1;
    secure_length = expire_start - secure_start - 1;
    expire_length = name_start - expire_start - 1;
    name_length = value_start - name_start - 1;
    value_length = line_length - value_start - 1;

    new_cookie[COL_DOMAIN] = calloc(domain_length + 1, sizeof(gchar));
    strncpy(new_cookie[COL_DOMAIN], line_ptr, domain_length);
    new_cookie[COL_DOMAIN][domain_length] = '\0';

    /* increment line_ptr so that it points to a shorter line */
    line_ptr += (domain_length + 1);
    
    new_cookie[COL_SUFFIX] = calloc(suffix_length + 1, sizeof(gchar));
    strncpy(new_cookie[COL_SUFFIX], line_ptr, suffix_length);
    new_cookie[COL_SUFFIX][suffix_length] = '\0';

    line_ptr += (suffix_length + 1);

    new_cookie[COL_PATH] = calloc(path_length + 1, sizeof(gchar));
    strncpy(new_cookie[COL_PATH], line_ptr, path_length);
    new_cookie[COL_PATH][path_length] = '\0';


    line_ptr += (path_length + 1);

    new_cookie[COL_SECURE] = calloc(secure_length + 1, sizeof(gchar));
    strncpy(new_cookie[COL_SECURE], line_ptr, secure_length);
    new_cookie[COL_SECURE][secure_length] = '\0';


    line_ptr += (secure_length + 1);

    new_cookie[COL_EXPIRE_SECONDS] = calloc(expire_length + 1, sizeof(gchar));
    strncpy(new_cookie[COL_EXPIRE_SECONDS], line_ptr, expire_length);
    new_cookie[COL_EXPIRE_SECONDS][expire_length] = '\0';


    line_ptr += (expire_length + 1);

    new_cookie[COL_NAME] = calloc(name_length + 1, sizeof(gchar));
    strncpy(new_cookie[COL_NAME], line_ptr, name_length);
    new_cookie[COL_NAME][name_length] = '\0';

    line_ptr += (name_length + 1);

    /* OK, this is a bit scary, but for some reason,
       we have to go one extra char here... otherwise,
       we lose the last char of the value! (which is the
       last char of the line, not counting the '\0') */
    new_cookie[COL_VALUE] = calloc(value_length + 2, sizeof(gchar));
    strncpy(new_cookie[COL_VALUE], line_ptr, value_length + 1);
    new_cookie[COL_VALUE][value_length + 1] = '\0';


    /* a human-readable date is created and placed in slot 7 */
    cookie_time = atoi(new_cookie[COL_EXPIRE_SECONDS]);
    /* cookie_tm_ptr = malloc(sizeof(struct tm)); seems unnecessary */
    cookie_tm_ptr = gmtime(&cookie_time);
    /* interestingly, asctime() always returns a string 26 gchars
       long, including a '\n' and a '\0' at the end. I think I'll
       turn the '\n' into a ' ' */
    new_cookie[COL_EXPIRE_DATE] = calloc(26, sizeof(gchar));
    strncpy(new_cookie[COL_EXPIRE_DATE], asctime(cookie_tm_ptr), 26);
    /* change the '\n' at slot 24 to a ' ' */
    new_cookie[COL_EXPIRE_DATE][24] = ' ';
    /* free(cookie_tm_ptr);  causes SIGSEGV */
}

static void free_cookie(gchar *c[COOKIE_COLUMNS]) {
    int i;
    for (i = 0; i < COOKIE_COLUMNS; i++) {
	if (c[i] != NULL) {
	    free(c[i]);
	    c[i] = NULL;
	}
    }
}

static void copy_cookie(gchar *c1[COOKIE_COLUMNS], gchar *c2[COOKIE_COLUMNS]) {
    /* COPIES c1 to c2 */
    
    /* ASSUMPTION: c1 and c2's pointers are all initialised, either to NULL
       or to a string of gchar */

    int i;
    size_t field_length;

    /* if c2 was pointing to anything, it won't be for long... */
    /* Remember: free_cookie frees any memory pointed to by c2
       as well as setting all of c2's pointers to null */
    free_cookie(c2);

    for (i = 0; i < COOKIE_COLUMNS; i++) {
	if (c1[i] != NULL) {
	    field_length = strlen(c1[i]);
	    c2[i] = calloc(field_length + 1, sizeof(gchar));
	    strncpy(c2[i], c1[i], field_length);
	    c2[i][field_length] = '\0';
	}
    }
    
}


static void copy_clist_row_to_cookie(gint row, gchar *c[COOKIE_COLUMNS]) {
    int i;
    size_t field_length;
    gchar *cell_contents;

    /* if c was pointing to anything, it won't be for long... */
    /* Remember: free_cookie frees any memory pointed to by c
       as well as setting all of c's pointers to null */
    free_cookie(c);

    /* if the row is larger than the number of rows in the clist,
       we can return right now */
    if (row >= glbl_clist_rows) {
	return;
    }

    for (i = 0; i < COOKIE_COLUMNS; i++) {
	gtk_clist_get_text(GTK_CLIST (glbl_clist), row, i, &cell_contents);
	if (cell_contents != NULL) {
	    field_length = strlen(cell_contents);
	    c[i] = calloc(field_length + 1, sizeof(gchar));
	    strncpy(c[i], cell_contents, field_length);
	    c[i][field_length] = '\0';
	}
    }
}


/*
 * GtkEntry
 */

static void show_edit_dialog_box (GtkWidget *calling_widget, gpointer func_data) {
    static GtkWidget *window = NULL;
    GtkWidget *box1;
    GtkWidget *box2;
    GtkWidget *table;
    GtkWidget *hbox;
    GtkWidget *text_entry[COOKIE_COLUMNS];

    GtkWidget *suffix_tf_checkbox, *secure_tf_checkbox;
    
    GtkWidget *OK_button;
    GtkWidget *Cancel_button;
    GtkWidget *separator;
    GtkWidget *label, *exp_s_label;
    gint row;
    row = glbl_clist_selected_row;

    if (!window) {
	window = gtk_window_new (GTK_WINDOW_TOPLEVEL);

	gtk_signal_connect (GTK_OBJECT (window), "destroy",
			    GTK_SIGNAL_FUNC(gtk_widget_destroyed),
			    &window);
	/* any keypress over this window calls handle_keypress_in_edit_win
	   so that when an <Enter> or <Esc> is detected, it calls what
	   the OK and Cancel buttons would do respectively */
	gtk_signal_connect(GTK_OBJECT(window), "key_press_event",
			   GTK_SIGNAL_FUNC(handle_keypress_in_edit_win),
			   NULL);

	gtk_window_set_title (GTK_WINDOW (window), "Edit Cookie");
	gtk_container_border_width (GTK_CONTAINER (window), 0);


	box1 = gtk_vbox_new (FALSE, 0);
	gtk_container_add (GTK_CONTAINER (window), box1);
	gtk_widget_show (box1);


	box2 = gtk_vbox_new (FALSE, 10);
	gtk_container_border_width (GTK_CONTAINER (box2), 10);
	gtk_box_pack_start (GTK_BOX (box1), box2, TRUE, TRUE, 0);
	gtk_widget_show (box2);

	table = gtk_table_new(9,
			      2,
                              FALSE);
	gtk_box_pack_start (GTK_BOX (box2), table, TRUE, TRUE, 0);

/* {COL_DOMAIN, COL_SUFFIX, COL_PATH, COL_SECURE, COL_EXPIRE_SECONDS, COL_NAME, COL_VALUE, COL_EXPIRE_DATE}; */

	/* COL_DOMAIN Name Edit Box */
	label = gtk_label_new(glbl_cookie_titles_str[COL_DOMAIN]);
 
	text_entry[COL_DOMAIN] = gtk_entry_new ();
 
	    /* I need to pass a pointer to an int as an argument for the
	       signal connect function. The int represents the
	       column of the bit of cookie data. That's what the
	       glbl_int_ptr[COL_DOMAIN] is for.
	       */
	
	gtk_signal_connect(GTK_OBJECT(text_entry[COL_DOMAIN]), "changed",
			   GTK_SIGNAL_FUNC(change_callback),
			   glbl_int_ptr[COL_DOMAIN]);
	gtk_entry_set_text (GTK_ENTRY (text_entry[COL_DOMAIN]),
			    glbl_selected_cookie[COL_DOMAIN]);

	gtk_table_attach_defaults(GTK_TABLE(table), label, 0, 1, 0, 1);
	gtk_table_attach_defaults(GTK_TABLE(table), text_entry[COL_DOMAIN], 1, 2, 0, 1);
	gtk_widget_show (label);
	gtk_widget_show (text_entry[COL_DOMAIN]);


/* {COL_DOMAIN, COL_SUFFIX, COL_PATH, COL_SECURE, COL_EXPIRE_SECONDS, COL_NAME, COL_VALUE, COL_EXPIRE_DATE}; */
	/* Name Edit Box */
	label = gtk_label_new(glbl_cookie_titles_str[COL_NAME]);
 
	text_entry[COL_NAME] = gtk_entry_new ();
 
	gtk_signal_connect(GTK_OBJECT(text_entry[COL_NAME]), "changed",
			   GTK_SIGNAL_FUNC(change_callback),
			   glbl_int_ptr[COL_NAME]);
	gtk_entry_set_text (GTK_ENTRY (text_entry[COL_NAME]),
			    glbl_selected_cookie[COL_NAME]);

	gtk_table_attach_defaults(GTK_TABLE(table), label, 0, 1, 1, 2);
	gtk_table_attach_defaults(GTK_TABLE(table), text_entry[COL_NAME], 1, 2, 1, 2);
	gtk_widget_show (label);
	gtk_widget_show (text_entry[COL_NAME]);


/* {COL_DOMAIN, COL_SUFFIX, COL_PATH, COL_SECURE, COL_EXPIRE_SECONDS, COL_NAME, COL_VALUE, COL_EXPIRE_DATE}; */
	/* Value Edit Box */
	label = gtk_label_new(glbl_cookie_titles_str[COL_VALUE]);
 
	text_entry[COL_VALUE] = gtk_entry_new ();
 
	gtk_signal_connect(GTK_OBJECT(text_entry[COL_VALUE]), "changed",
			   GTK_SIGNAL_FUNC(change_callback),
			   glbl_int_ptr[COL_VALUE]);
	gtk_entry_set_text (GTK_ENTRY (text_entry[COL_VALUE]),
			    glbl_selected_cookie[COL_VALUE]);

	gtk_table_attach_defaults(GTK_TABLE(table), label, 0, 1, 2, 3);
	gtk_table_attach_defaults(GTK_TABLE(table), text_entry[COL_VALUE], 1, 2, 2, 3);
	gtk_widget_show (label);
	gtk_widget_show (text_entry[COL_VALUE]);


/* {COL_DOMAIN, COL_SUFFIX, COL_PATH, COL_SECURE, COL_EXPIRE_SECONDS, COL_NAME, COL_VALUE, COL_EXPIRE_DATE}; */
	/* COL_PATH Edit Box */
	label = gtk_label_new(glbl_cookie_titles_str[COL_PATH]);
 
	text_entry[COL_PATH] = gtk_entry_new ();
 
	gtk_signal_connect(GTK_OBJECT(text_entry[COL_PATH]), "changed",
			   GTK_SIGNAL_FUNC(change_callback),
			   glbl_int_ptr[COL_PATH]);
	gtk_entry_set_text (GTK_ENTRY (text_entry[COL_PATH]),
			    glbl_selected_cookie[COL_PATH]);

	gtk_table_attach_defaults(GTK_TABLE(table), label, 0, 1, 3, 4);
	gtk_table_attach_defaults(GTK_TABLE(table), text_entry[COL_PATH], 1, 2, 3, 4);
	gtk_widget_show (label);
	gtk_widget_show (text_entry[COL_PATH]);
	

/* {COL_DOMAIN, COL_SUFFIX, COL_PATH, COL_SECURE, COL_EXPIRE_SECONDS, COL_NAME, COL_VALUE, COL_EXPIRE_DATE}; */
	/* COL_SUFFIX T/F Edit Box */

	suffix_tf_checkbox = gtk_check_button_new_with_label ("Domain field is actually a suffix");
 
	gtk_signal_connect(GTK_OBJECT(suffix_tf_checkbox), "clicked",
			   GTK_SIGNAL_FUNC(change_tf_callback),
			   glbl_int_ptr[COL_SUFFIX]);

	/* turn the button on or off depending on whether the
	   cookie field is "TRUE" or "FALSE" */
	if (strcmp(glbl_selected_cookie[COL_SUFFIX], "TRUE") == 0) {
	    /* checkbox is checked */
	    gtk_toggle_button_set_state( GTK_TOGGLE_BUTTON(suffix_tf_checkbox),
					 TRUE );    
	} else {
	    /* checkbox is unchecked */
	    gtk_toggle_button_set_state( GTK_TOGGLE_BUTTON(suffix_tf_checkbox),
					 FALSE );    
	}

	gtk_table_attach_defaults(GTK_TABLE(table), suffix_tf_checkbox, 0, 2, 4, 5);
	
	gtk_widget_show (suffix_tf_checkbox);

/* {COL_DOMAIN, COL_SUFFIX, COL_PATH, COL_SECURE, COL_EXPIRE_SECONDS, COL_NAME, COL_VALUE, COL_EXPIRE_DATE}; */
	/* COL_SECURE T/F Edit Box */

	secure_tf_checkbox = gtk_check_button_new_with_label ("Make this cookie secure");
 
	gtk_signal_connect(GTK_OBJECT(secure_tf_checkbox), "clicked",
			   GTK_SIGNAL_FUNC(change_tf_callback),
			   glbl_int_ptr[COL_SECURE]);

	/* turn the button on or off depending on whether the
	   cookie field is "TRUE" or "FALSE" */
	if (strcmp(glbl_selected_cookie[COL_SECURE], "TRUE") == 0) {
	    /* checkbox is checked */
	    gtk_toggle_button_set_state( GTK_TOGGLE_BUTTON(secure_tf_checkbox),
					 TRUE );    
	} else {
	    /* checkbox is unchecked */
	    gtk_toggle_button_set_state( GTK_TOGGLE_BUTTON(secure_tf_checkbox),
					 FALSE );    
	}

	gtk_table_attach_defaults(GTK_TABLE(table), secure_tf_checkbox, 0, 2, 5, 6);
	
	gtk_widget_show (secure_tf_checkbox);


	/* Descriptive label above the human-readable date dialogue box */

	label = gtk_label_new("\nCookie expiration dates are stored as\nthe number of seconds since 1 Jan 1970.\nAs you edit the human-readble date below,\nstick to the following format:\nWdy Mon dd hh:mm:ss yyyy and\ngtkcookie will be able to figure\nout the time in seconds for you.\n");
 
	gtk_widget_show (label);
	gtk_table_attach_defaults(GTK_TABLE(table), label, 0, 2, 6, 7);

/* {COL_DOMAIN, COL_SUFFIX, COL_PATH, COL_SECURE, COL_EXPIRE_SECONDS, COL_NAME, COL_VALUE, COL_EXPIRE_DATE}; */
	/* Expiry Date (in seconds) Label */
	label = gtk_label_new(glbl_cookie_titles_str[COL_EXPIRE_SECONDS]);
	exp_s_label = gtk_label_new(glbl_selected_cookie[COL_EXPIRE_SECONDS]);
	/* have our global GtkWidget pointer point to the expiry date
	   label, so that we can change the label even after this function
	   has run */
	glbl_selected_etl = exp_s_label;
 
	gtk_table_attach_defaults(GTK_TABLE(table), label, 0, 1, 7, 8);
	gtk_table_attach_defaults(GTK_TABLE(table), exp_s_label, 1, 2, 7, 8);
	gtk_widget_show (label);
	gtk_widget_show (exp_s_label);


/* {COL_DOMAIN, COL_SUFFIX, COL_PATH, COL_SECURE, COL_EXPIRE_SECONDS, COL_NAME, COL_VALUE, COL_EXPIRE_DATE}; */
	/* COL_EXPIRE_DATE Edit Box */
	label = gtk_label_new(glbl_cookie_titles_str[COL_EXPIRE_DATE]);
 
	text_entry[COL_EXPIRE_DATE] = gtk_entry_new ();
 
	/* have to find a way to pass a pointer to the expiry date in seconds
	   label to this function, so that the function can change the label.
	   In the function itself, use the Linux-specific call strptime()
	   to take care of parsing the date as it is edited. */
	gtk_signal_connect(GTK_OBJECT(text_entry[COL_EXPIRE_DATE]), "changed",
			   GTK_SIGNAL_FUNC(change_time_callback),
			   glbl_int_ptr[COL_EXPIRE_DATE]);
	gtk_entry_set_text (GTK_ENTRY (text_entry[COL_EXPIRE_DATE]),
			    glbl_selected_cookie[COL_EXPIRE_DATE]);

	gtk_table_attach_defaults(GTK_TABLE(table), label, 0, 1, 8, 9);
	gtk_table_attach_defaults(GTK_TABLE(table), text_entry[COL_EXPIRE_DATE], 1, 2, 8, 9);
	gtk_widget_show (label);
	gtk_widget_show (text_entry[COL_EXPIRE_DATE]);

	gtk_widget_show (table);

	/* if we're done with the cookie here, nuke it and
	   see what happens */
	/* free_cookie(glbl_selected_cookie); */
	/* OK, what happens is that the cookie really does contain
	   pointers right into the glbl_clist's elements. Freeing the memory
	   that the cookie's gchar pointers point to fucks up the
	   elements in the glbl_clist */

	/* To the best of my knowlege, gtk_entry_set_text copies
	   the string from the pointer into the text widget, but
	   de-allocation of the text seems to be the responsibility
	   of the widget */
	
	separator = gtk_hseparator_new ();
	gtk_box_pack_start (GTK_BOX (box1), separator, FALSE, TRUE, 0);
	gtk_widget_show (separator);

/* buttons at bottom */

	box2 = gtk_vbox_new (FALSE, 10);
	gtk_container_border_width (GTK_CONTAINER (box2), 10);
	gtk_box_pack_start (GTK_BOX (box1), box2, FALSE, TRUE, 0);
	gtk_widget_show (box2);


	
	hbox = gtk_hbox_new (TRUE, 10);
	

	OK_button = gtk_button_new_with_label ("OK");
	/* note that I pass a pointer to this window as the second arg of
	   this function, because update_glbl_clist() destroys
	   the widget in arg2. This is useful, because in this case,
	   I want the edit window to be destroyed after the OK
	   button is clicked. */
	gtk_signal_connect (GTK_OBJECT (OK_button), "clicked",
			    GTK_SIGNAL_FUNC(update_glbl_clist),
			    GTK_OBJECT(window));
	gtk_box_pack_start (GTK_BOX (hbox), OK_button, TRUE, TRUE, 0);
	GTK_WIDGET_SET_FLAGS (OK_button, GTK_CAN_DEFAULT);
	gtk_widget_grab_default (OK_button);
	gtk_widget_show (OK_button);
	gtk_widget_show (hbox);


	Cancel_button = gtk_button_new_with_label ("Cancel");
	gtk_signal_connect_object (GTK_OBJECT (Cancel_button), "clicked",
				   GTK_SIGNAL_FUNC(gtk_widget_destroy),
				   GTK_OBJECT (window));
	gtk_box_pack_start (GTK_BOX (hbox), Cancel_button, TRUE, TRUE, 0);
	GTK_WIDGET_SET_FLAGS (Cancel_button, GTK_CAN_DEFAULT);
	gtk_widget_show (Cancel_button);
	gtk_widget_show (hbox);

	gtk_box_pack_start (GTK_BOX (box2), hbox, TRUE, TRUE, 0);
    }

    if (!GTK_WIDGET_VISIBLE (window)) {
	gtk_widget_show (window);
    } else {
	gtk_widget_destroy (window);
    }
}


static void change_callback(GtkWidget *entry, gpointer func_data) {
    gchar *entry_text;
    int i;  /* the number of the cookie column */
    size_t field_length;

    i = *(int *) func_data;
    entry_text = gtk_entry_get_text(GTK_ENTRY(entry));

    if (entry_text != NULL) {
	field_length = strlen(entry_text);
	/* shouldn't I free the memory before re-allocating it? YES! */
	if (glbl_selected_cookie[i] != NULL) {
	    free(glbl_selected_cookie[i]);
	}
	glbl_selected_cookie[i] = calloc(field_length + 1, sizeof(gchar));
	strncpy(glbl_selected_cookie[i], entry_text, field_length);
	glbl_selected_cookie[i][field_length] = '\0';
    }
}


static void change_time_callback(GtkWidget *entry, gpointer func_data) {
    gchar *entry_text;
    int i;  /* the number of the cookie column */
    size_t field_length;

    i = *(int *) func_data;
    entry_text = gtk_entry_get_text(GTK_ENTRY(entry));

    if (entry_text != NULL) {
	field_length = strlen(entry_text);
	/* shouldn't I free the memory before re-allocating it? YES! */
	if (glbl_selected_cookie[i] != NULL) {
	    free(glbl_selected_cookie[i]);
	}
	glbl_selected_cookie[i] = calloc(field_length + 1, sizeof(gchar));
	strncpy(glbl_selected_cookie[i], entry_text, field_length);
	glbl_selected_cookie[i][field_length] = '\0';

	strptime(glbl_selected_cookie[i], "%a %b %d %T %Y", &glbl_selected_time);
	glbl_selected_time_sec = mktime(&glbl_selected_time);
	sprintf(glbl_selected_time_str, "%i", (int)glbl_selected_time_sec);
	gtk_label_set( GTK_LABEL(glbl_selected_etl), glbl_selected_time_str);
	/* gtk_label_set( GTK_LABEL(glbl_selected_etl), glbl_selected_cookie[4]); */

	/* remember, below, that glbl_selected_cookie[4] is the date in seconds */
	if (glbl_selected_time_str != NULL) {
	    field_length = strlen(glbl_selected_time_str);
	    /* shouldn't I free the memory before re-allocating it? YES! */
	    if (glbl_selected_cookie[4] != NULL) {
		free(glbl_selected_cookie[4]);
	    }
	    glbl_selected_cookie[4] = calloc(field_length + 1, sizeof(gchar));
	    strncpy(glbl_selected_cookie[4], glbl_selected_time_str, field_length);
	    glbl_selected_cookie[4][field_length] = '\0';
	}
    }
}

static void change_tf_callback(GtkWidget *toggle_button, gpointer func_data) {
    int i;  /* the number of the cookie column */
    size_t field_length;
    gchar *True = "TRUE";
    gchar *False = "FALSE";
    gchar *Bool = NULL;
    
    i = *(int *) func_data;

    if (GTK_TOGGLE_BUTTON (toggle_button)->active) {
	Bool = True;
    } else {
	Bool = False;
    }
    field_length = strlen(Bool);
    /* shouldn't I free the memory before re-allocating it? YES! */
    if (glbl_selected_cookie[i] != NULL) {
	free(glbl_selected_cookie[i]);
    }
    glbl_selected_cookie[i] = calloc(field_length + 1, sizeof(gchar));
    strncpy(glbl_selected_cookie[i], Bool, field_length);
    glbl_selected_cookie[i][field_length] = '\0';
}

/* static void update_clist(GtkWidget *clist, gpointer window) { */
static void update_glbl_clist(GtkWidget *calling_widget,
			      GtkWidget *edit_win_to_destroy) {
    int column;
    for (column = 0; column < COOKIE_COLUMNS; column++) {
	gtk_clist_set_text(GTK_CLIST(glbl_clist), glbl_clist_selected_row, column,
                               glbl_selected_cookie[column] );
    }
    glbl_last_sorted_column = -1;  /* because we just set the text
				      in a column somewhere
				      in the clist, no columns
				      can be assumed to be
				      sorted anymore */

    gtk_widget_destroy(edit_win_to_destroy);
}


/* FILE SELECTION STUFF */


static void file_selection_ok (GtkWidget *w, GtkFileSelection *fs) {
    size_t length;
    gchar *filename;
    filename = gtk_file_selection_get_filename (GTK_FILE_SELECTION (fs));
    if (glbl_selected_filename_str != NULL) {
	free(glbl_selected_filename_str);
    }
    if (filename != NULL) {
	length = strlen(filename);
	glbl_selected_filename_str = calloc(length + 1, sizeof(gchar));
	strncpy(glbl_selected_filename_str, filename, length);
	glbl_selected_filename_str[length] = '\0';
    }
    gtk_widget_destroy (GTK_WIDGET (fs));
    /* clear the glbl_clist */
    clear_glbl_clist();
    populate_glbl_clist();
}

static void show_open_file_dialog_box (GtkWidget *calling_widget, gpointer func_data) {
    static GtkWidget *window = NULL;

    if (!window) {
	window = gtk_file_selection_new ("Open");

	gtk_file_selection_hide_fileop_buttons (GTK_FILE_SELECTION (window));

	gtk_window_position (GTK_WINDOW (window), GTK_WIN_POS_MOUSE);

	gtk_signal_connect (GTK_OBJECT (window), "destroy",
			    GTK_SIGNAL_FUNC(gtk_widget_destroyed),
			    &window);

	gtk_signal_connect (GTK_OBJECT (GTK_FILE_SELECTION (window)->ok_button),
			    "clicked", GTK_SIGNAL_FUNC(file_selection_ok),
			    window);
	gtk_signal_connect_object (GTK_OBJECT (GTK_FILE_SELECTION (window)->cancel_button),
				   "clicked", GTK_SIGNAL_FUNC(gtk_widget_destroy),
				   GTK_OBJECT (window));
      
    }
  
    if (!GTK_WIDGET_VISIBLE (window)) {
	gtk_widget_show (window);
    } else {
	gtk_widget_destroy (window);
    }
}


static void show_save_as_dialog_box (GtkWidget *calling_widget, gpointer func_data) {
    static GtkWidget *window = NULL;

    if (!window) {
	window = gtk_file_selection_new ("Save As");

	gtk_file_selection_hide_fileop_buttons (GTK_FILE_SELECTION (window));

	gtk_window_position (GTK_WINDOW (window), GTK_WIN_POS_MOUSE);

	gtk_signal_connect (GTK_OBJECT (window), "destroy",
			    GTK_SIGNAL_FUNC(gtk_widget_destroyed),
			    &window);

	gtk_signal_connect (GTK_OBJECT (GTK_FILE_SELECTION (window)->ok_button),
			    "clicked", GTK_SIGNAL_FUNC(save_as_ok),
			    window);
	gtk_signal_connect_object (GTK_OBJECT (GTK_FILE_SELECTION (window)->cancel_button),
				   "clicked", GTK_SIGNAL_FUNC(gtk_widget_destroy),
				   GTK_OBJECT (window));

	if (glbl_selected_filename_str != NULL) {
	    gtk_file_selection_set_filename ( GTK_FILE_SELECTION(window), glbl_selected_filename_str);
	}
    }
  
    if (!GTK_WIDGET_VISIBLE (window)) {
	gtk_widget_show (window);
    } else {
	gtk_widget_destroy (window);
    }
}


static void save_as_ok (GtkWidget *w, GtkFileSelection *fs) {
    size_t length;
    gchar *filename;
    filename = gtk_file_selection_get_filename (GTK_FILE_SELECTION (fs));
    if (glbl_selected_filename_str != NULL) {
	free(glbl_selected_filename_str);  /* don't I set to null as well? 55555 */
    }
    if (filename != NULL) {
	length = strlen(filename);
	glbl_selected_filename_str = calloc(length + 1, sizeof(gchar));
	strncpy(glbl_selected_filename_str, filename, length);
	glbl_selected_filename_str[length] = '\0';
    }
    gtk_widget_destroy (GTK_WIDGET (fs));

    /* set the title of the main window to show the selected filename */
    /* first, erase what's currently in the gstring */
    g_string_erase(glbl_window_title_gstr, 0, glbl_window_title_gstr->len);
    /* now create the new title, always starting with "gtkcookie - " and
       then the filename */
    g_string_assign(glbl_window_title_gstr, "gtkcookie - ");
    g_string_append(glbl_window_title_gstr, glbl_selected_filename_str);
    /* set the title of the main window */
    gtk_window_set_title(GTK_WINDOW (glbl_main_window), glbl_window_title_gstr->str);

    /* now that glbl_selected_filename_str has got the new value,
       call save_file(), which will save glbl_selected_filename_str */
    /* note too that save_file is a callback function, which means
       that it can be passed a pointer to the calling widget and to
       extra function data, neither of which we need here; hence,
       the two NULLs */
    save_file(NULL, NULL);
}

static void check_for_running_netscape(void) {
    uid_t uid_of_this_process;
    gboolean error;  /* was there an error trying to find
			a process named "netscape"? */
    pid_t netscape_pid = 0;
    GString *error_msg_gstr;

    uid_of_this_process = getuid();
    /* if a "netscape" is running under the same uid of this
       programme, then we know the user is running netscape */
    netscape_pid = process_is_running("netscape", uid_of_this_process, &error);
    /* note that Red Had actually runs netscape as "netscape-commun",
       so check for that too, if the first check revealed nothing */
    if (netscape_pid == 0) {
	netscape_pid = process_is_running("netscape", uid_of_this_process, &error);
    }
	
    if (netscape_pid != 0) {
	error_msg_gstr = g_string_new("");
	g_string_sprintf(error_msg_gstr, "It looks like you're running Netscape at pid %i right now.\nPlease note that your changes to your netscape cookie file\nwill be lost if you leave Netscape running.\nNetscape writes the cookie file just before exiting,\nand could therefore clobber any changes you make to the file now.", netscape_pid);
	show_error_dialog_box(error_msg_gstr->str);
	/* don't forget to deallocate the string after we're done
	   with it! Fortunately, deallocating the string seems not
	   to harm the error message created by show_error_dialog_box() */
	g_string_free(error_msg_gstr, TRUE);
    }
}

static void
search (GtkWidget* wdg, gpointer data) {
    gint row;
    gint column;
	gchar *needle, *cell_contents;

    /* time to try to use gstrings, methinks... */
    /* oooh, gstrings are sexy! */
    GString *cell_str;  /* the string from a cell in the clist */
    GString *find_str;  /* the string in the "find" text entry box */

	needle = (gchar*) gtk_entry_get_text (GTK_ENTRY (data));
	if (!strlen (needle)) {
		return;
	}
	find_str = g_string_new (needle);

    /* if the search is case-insensitive, make everything in
       find_str uppercase */
    if (glbl_search_case_sensitive == FALSE) {
	g_string_up(find_str);
    }
    
    for (row = glbl_clist_selected_row; row < glbl_clist_rows; row++) {
	for (column = 0; column < COOKIE_COLUMNS; column++) {
	    gtk_clist_get_text(GTK_CLIST (glbl_clist), row, column,
			       &cell_contents);
	    
	    /* we need to copy construct cell_str, so that when we
	       uppercase cell_str, we are not uppercasing the actual
	       clist cell data that cell_str points to! */
	    cell_str = g_string_new("");
	    g_string_append(cell_str, cell_contents);

	    /* if the search is case-insensitive, make everything in
	       cell_str uppercase */
	    if (glbl_search_case_sensitive == FALSE) {
		g_string_up(cell_str);
	    }
	    
	    if (strstr(cell_str->str, find_str->str) != NULL
		&& row > glbl_last_found_row) {
		/* in the if statement directly above, we don't count
		   a search that is successful in the last found row,
		   because that search has already been done, and the
		   user is interested in rows AFTER the current row.
		   In the case of a match after the last found row,
		   set the last found row to the currently matched row
		   so that the number is accurate for the next time
		   this function is called */
		glbl_last_found_row = row;
		gtk_clist_unselect_row(GTK_CLIST(glbl_clist), row, column);
		/* manni: perhaps if you don't check for visibility, but
		   simply always do the moveto, you will conquer the
		   bug where a row is sometimes just off the display
		   area (due to the bottom scrollbar?) */
		if (gtk_clist_row_is_visible(GTK_CLIST(glbl_clist), row) ==
					     GTK_VISIBILITY_NONE) {
		    gtk_clist_moveto(GTK_CLIST(glbl_clist), row, column,
				     0.5, 0.5);
		}
		gtk_clist_select_row(GTK_CLIST(glbl_clist), row, column);
		select_clist(glbl_clist, row, column, NULL);
		/* DEALLOCATE! */
		g_string_free(cell_str, TRUE);
		g_string_free(find_str, TRUE);
		return;
	    }
	    /* DEALLOCATE! */
	    g_string_free(cell_str, TRUE);
	}
    }
    /* DEALLOCATE! */
    g_string_free(find_str, TRUE);
    
    /* if we made it this far, we must have a not found condition! */
    show_error_dialog_box("Not found.");
}

static void
clear_search (GtkWidget* wdg, gpointer data) {
	gtk_entry_set_text (GTK_ENTRY (data), "");
    /* the last found row is rendered invalid when the user clears
       the text box, so reset that variable to -1 */
    glbl_last_found_row = -1;
}

static void reset_last_found_row(GtkWidget *calling_widget, gpointer func_data) {
    glbl_last_found_row = -1;
}

/* seems not to be working
static void set_cursor (guint c) {
    GdkCursor *cursor;

    c = CLAMP (c, 0, 152);
    c &= 0xfe;

    cursor = gdk_cursor_new (c);
    gdk_window_set_cursor (glbl_main_window->window, cursor);
    gdk_cursor_destroy (cursor);
}
*/

static void quicksort_clist(GtkWidget *clist,
			    SORT_STYLE sort_style,
			    gint sort_column,
			    gint first_index,
			    gint last_index) {

    /* clist is the clist */
    /* sort column is the column that we are sorting on */
    /* first_index is the index of the first element of the (sub)array */
    /* n is the number of elements in the clist */
    gint pivot_index;  /* index of the pivot element */

    if (first_index < last_index) {
	/* partition the array and set the pivot_index */
	partition_clist(clist, sort_style, sort_column, first_index, &pivot_index, last_index);

	/* recursive calls will now sort the subarrays */
	quicksort_clist(clist, sort_style, sort_column, first_index, (pivot_index - 1));
	quicksort_clist(clist, sort_style, sort_column, (pivot_index + 1), last_index);
    }
}

static void partition_clist(GtkWidget *clist,
			    SORT_STYLE sort_style,
			    gint sort_column,
			    gint first_index,
			    gint *pivot_index,
			    gint last_index) {

    gchar *pivot_cookie[COOKIE_COLUMNS];
    gchar *sort_column_cell_contents;  /* text inside the cell to be sorted */
    gchar *cell_contents;  /* generic contents of any cell */
    gchar *more_cell_contents;  /* generic contents of any cell */
    gint col;  /* column */
    int (*compfunction)(const char*, const char*); /* pointer to
						      a comparison function
						      that takes two strings
						      as its args */

    gint too_big_index = first_index + 1;
    gint too_small_index = last_index;  /* hopefully this makes
					   the code more readable,
					   not less -- besides which,
					last_index isn't allowed to
					change, whereas too_small_index
					will be changing a lot */

    initialise_cookie(pivot_cookie);  /* always do this with a new cookie! */

    /* choose the first cookie as the pivot_cookie */
    copy_clist_row_to_cookie(first_index, pivot_cookie);

    /* make the comparison function either strcmp from the
     standard C library, or intcmp, defined by me in this code. */
    if (sort_style == STRING) {
	compfunction = strcmp;
    } else {
	compfunction = intcmp;
    }
    while (too_big_index <= too_small_index) {
	gtk_clist_get_text(GTK_CLIST (clist), too_big_index, sort_column,
			   &sort_column_cell_contents);
	/* with strcmp, instead use a pointer to some function
	   depending on the sort column, that way we can have the
	   function return true or false depending on whatever comparison
	   is necessary for the sort column. This way, we can do
	   a strcmp for strings, but a atol on both strings, and
	   a numeric comparison for dates */
	/* SORT: strcmp <= 0 */
	/* while (too_big_index <= last_index && strcmp(sort_column_cell_contents, pivot_cookie[sort_column]) <= 0) { */
	while (too_big_index <= last_index && (*compfunction)(sort_column_cell_contents, pivot_cookie[sort_column]) <= 0) {
	    ++too_big_index;
	    gtk_clist_get_text(GTK_CLIST (clist), too_big_index, sort_column,
			       &sort_column_cell_contents);
	}
	gtk_clist_get_text(GTK_CLIST (clist), too_small_index, sort_column,
			   &sort_column_cell_contents);
	/* SORT: strcmp > 0 */
	/* while (strcmp(sort_column_cell_contents, pivot_cookie[sort_column]) > 0) { */
	while ((*compfunction)(sort_column_cell_contents, pivot_cookie[sort_column]) > 0) {
	    --too_small_index;
	    gtk_clist_get_text(GTK_CLIST (clist), too_small_index, sort_column,
			       &sort_column_cell_contents);
	}
	if (too_big_index < too_small_index) {
	    swap_row_clist(clist, too_big_index, too_small_index);
	    
	    gtk_clist_get_text(GTK_CLIST (clist), too_big_index, sort_column,
			       &cell_contents);
	    gtk_clist_get_text(GTK_CLIST (clist), too_small_index, sort_column,
			       &more_cell_contents);
	}
    }

    *pivot_index = too_small_index;
    /* move the data at the pivot_index (which still contains the value that is
       less than or equal to the pivot) to the first slot. Unless, of course,
       first_index and *pivot_index are already pointing to the same slot. x*/
    if (first_index != *pivot_index) {
	for (col = 0; col < COOKIE_COLUMNS; col++) {
	    gtk_clist_get_text(GTK_CLIST (clist), *pivot_index, col,
			       &cell_contents);
	    gtk_clist_set_text(GTK_CLIST (clist), first_index, col,
			       cell_contents);
	}
    }
    /* move what's in the pivot_cookie to the *pivot_index in the clist */
    for (col = 0; col < COOKIE_COLUMNS; col++) {
	gtk_clist_set_text(GTK_CLIST (clist), *pivot_index, col,
			   pivot_cookie[col]);
    }
    
    /* DEALLOCATE pivot_cookie */
    free_cookie(pivot_cookie);
}


/* set case sensitive searching TRUE/FALSE */
/* (held in the global variable glbl_search_case_sensitive) */
static void set_tf_case_sensitive_search(GtkWidget *toggle_button, gpointer func_data) {
    if (GTK_TOGGLE_BUTTON (toggle_button)->active) {
	glbl_search_case_sensitive = TRUE;
    } else {
	glbl_search_case_sensitive = FALSE;
    }
}

static gint
search_dlg_key_cb (GtkWidget* wdg, GdkEventKey* ev, gpointer data) {
	if (ev->keyval == GDK_Return) {
		search (wdg, data);
		return TRUE;
	} else if (ev->keyval == GDK_Escape) {
		gtk_widget_destroy (wdg);
	return TRUE;  /* tell calling code that we have handled this event */
    }
    return FALSE;
}


gint handle_keypress_in_edit_win(GtkWidget *edit_window, GdkEventKey *event, gpointer func_data) {
    if (event->keyval == GDK_Return) {
	/* note how the first arg below is NULL. update_glbl_clist
	   often gets called as a callback function, so a pointer to
	   the calling widget is generally passed as the first argument.
	   But here, I'm calling it manually, so I just leave the first
	   arg NULL. Doesn't seem to break anything, either. */
	update_glbl_clist(NULL, edit_window);
	return TRUE;  /* tell calling code that we have handled this event */
    }
    if (event->keyval == GDK_Escape) {
	gtk_widget_destroy(edit_window);
	return TRUE;  /* tell calling code that we have handled this event */
    }
    return FALSE;
}


gint handle_keypress_in_main_win(GtkWidget *calling_widget, GdkEventKey *event, gpointer func_data) {
    if (event->keyval == GDK_Delete) {
	/* note arg1 below is null, because with callback functions,
	   arg1 is automatically populated with a pointer to the
	   calling widget;
	   arg2 is null because it's a pointer to any function data
	   you may need to pass to the function... which we don't */
	remove_selected_row_from_glbl_clist(NULL, NULL);
	return TRUE;  /* tell calling code that we have handled this event */
    }
    return FALSE;
}

/* end */

