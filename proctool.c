/*
  proctool.c --> tool for finding processes on the system
  
  Copyright (c) 1998 Manni Wood

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

  Manni Wood: mwood@sig.bsh.com, pq1036@110.net

 */

#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>
#include <errno.h>
#include <sys/types.h>  /* defines pid_t, etc */
#include <glib.h>
#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>  /* contains all keyboard defs */
#include <ctype.h>  /* isdigit() needed by string_contains_all_numbers */
#include "proctool.h"
#include "getline.h"

static gboolean string_contains_all_numbers(const gchar *str);
static gboolean statusfile_contains(const gchar *filename,
				    const gchar *procname,
				    uid_t user_id);
static gboolean	nameline_matches_string(const gchar *file_line,
					const gchar *procname);
static gboolean uidline_matches_string(const gchar *file_line,
				       uid_t user_id);
static gboolean line_is_field(const gchar *file_line,
			      const gchar *fieldname);


pid_t process_is_running(const gchar *procname,
			 uid_t user_id,
			 gboolean *checkerrno) {

    DIR *dir;
    struct dirent *ent;
    pid_t netscape_pid = 0; /* ensure this is a default value
			       of 0, so that if it isn't assigned to,
			       it will at least have the "failure" value */
    GString *statusfilename;

    *checkerrno = FALSE;  /* default: no errors encountered */

    if (!(dir = opendir("/proc"))) {
	*checkerrno = TRUE;
	return 0;
    }

    /* reset errno to 0, so we can tell when readdir() fails */
    errno = 0;

    while ((ent = readdir(dir))) {
	/* each directory inside /proc that is a number, is a process
	   number directory */
	if (string_contains_all_numbers(ent->d_name)) {
	    statusfilename = g_string_new("/proc/");
	    g_string_append(statusfilename, ent->d_name);
	    g_string_append(statusfilename, "/status");
	    /* statusfilename should now equal something like
	       /proc/89/status, which is the filename
	       of a text document holding status info on pid 89 */
	    if (statusfile_contains(statusfilename->str, procname, user_id)) {
		/* turn the directory name into an int */
		netscape_pid = atoi(ent->d_name);
	    }
	    g_string_free(statusfilename, TRUE);
	}
    }

    if (errno) {
	*checkerrno = TRUE;
	return 0;
    }

    closedir(dir);

    return netscape_pid;
}

static gboolean string_contains_all_numbers(const gchar *str) {
    size_t i;

    i = 0;
    while (str[i] != '\0') {
	if (!(isdigit(str[i]))) {
	    return FALSE;
	}
	++i;
    }
    return TRUE;
}

static gboolean statusfile_contains(const gchar *filename,
				    const gchar *procname,
				    uid_t user_id) {

    /* so here, we have to open up the file, read it in, and look for
       a line that starts with Name:\tnetscape, and another line
       that starts with Uid:\t1277\t1277\t1277\t1277.
       Do a string match on the Name:, and turn the Uid: into an int
       and do a numeric equality comparison. If both are true, return
       TRUE, else return FALSE. */

    FILE *statfile = NULL;
    /* were we successful reading a line from the file? */
    gboolean success = FALSE;
    /* line of text from a file */
    gchar *file_line = NULL;
    /* did the process name match *procname? */
    gboolean match_procname = FALSE;
    /* did the Uid: match user_id? */
    gboolean match_uid = FALSE;

    statfile = fopen(filename, "r");
    if (statfile == NULL) {
	/* if we couldn't open the status file,
	   let's assume for now that the string
	   isn't in the file */
	printf("cannot read file \"%s\"\n", filename);
	/* BUT REALLY, WE WILL NEED MUCH NICER ERROR MESSAGE
	   HANDLING AND PASSING HERE */
	return FALSE;
    }
    
    file_line = get_line_safely(statfile, &success);
    if (success) {
	if (line_is_field(file_line, "Name:") &&
	    nameline_matches_string(file_line, procname)) {
	    match_procname = TRUE;
	}
	if (line_is_field(file_line, "Uid:") &&
	    uidline_matches_string(file_line, user_id)) {
	    match_uid = TRUE;
	}
    }
    free(file_line);
    file_line = NULL;

    while (success) {
	file_line = get_line_safely(statfile, &success);
	if (success) {
	    if (line_is_field(file_line, "Name:") &&
		nameline_matches_string(file_line, procname)) {
		match_procname = TRUE;
	    }
	    if (line_is_field(file_line, "Uid:") &&
		uidline_matches_string(file_line, user_id)) {
		match_uid = TRUE;
	    }
	}
	free(file_line);
	file_line = NULL;
    }

    fclose(statfile);

    return match_procname && match_uid;
}

static gboolean	nameline_matches_string(const gchar *file_line,
					const gchar *procname) {
    /* a name line is of the format:
       Name:\tprocname\0
    */
    /* here's a cute bit o' tricky code:
       I want to start comparing file_line from the 6th character,
       because "Name:\t" is 6 chars before the first char of
       the actual process name. So, I add 6 to the pointer file_line,
       and suddenly the pointer is pointing to the first char
       of the process name. Hence, we can accurately compare
       file_line + 6 with procname
    */
    if (strcmp(file_line + 6, procname) == 0) {
	return TRUE;
    } else {
	return FALSE;
    }
}
 
static gboolean uidline_matches_string(const gchar *file_line,
				       uid_t user_id) {
    /* a uidline reads:
       Uid:\t\d\t\d\t\d\t\d\0
    */
    gchar *uidstr;
    uid_t this_uid;
    size_t i;
    size_t uid_offset = 5; /* the offset to the first char of
			      uid */
    size_t number_of_tabs_found = 0;
    size_t line_length; /* length of file_line */
    size_t tab_locations[4]; /* there are 4 tabs in the uidline,
				so let's track where each one is */

    line_length = strlen(file_line);

    for (i = 0; i <= line_length; i++) {
	if (file_line[i] == '\t') {
	    tab_locations[number_of_tabs_found] = i;
	    ++number_of_tabs_found;
	    /* DON'T even think about trying to assign an extra tab
	       past the bounds of the array! So if there are too many
	       tabs, break out of the loop! */
	    if (number_of_tabs_found >= 4) {
		break;
	    }
	}
    }
    /* if there aren't 4 tabs, this isn't a valid line! */
    if (number_of_tabs_found != 4) {
	return FALSE;
    }

    /* get length and malloc! */
    uidstr = calloc(tab_locations[1] - uid_offset + 1, sizeof(gchar));
    uidstr = strncpy(uidstr, file_line + uid_offset, tab_locations[1] - uid_offset);
    /* don't forget the final \0 */
    uidstr[tab_locations[1] - uid_offset] = '\0';
    this_uid = atoi(uidstr);
    /* DEALLOCATE! */
    free(uidstr);
    if (this_uid == user_id) {
	return TRUE;
    } else {
	return FALSE;
    }
}
 
static gboolean line_is_field(const gchar *file_line,
			      const gchar *fieldname) {
    size_t length;

    length = strlen(fieldname);
    /* in other words, find out how long the field is,
       and only compare that many characters of file_line
       to fieldname */
    if (strncmp(file_line, fieldname, length) == 0) {
	return TRUE;
    } else {
	return FALSE;
    }
}



