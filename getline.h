#ifndef GETLINE_H
#define GETLINE_H
/*
  getline.h --> function for getting a line of any length
  from a filehandle
  
  Copyright (c) 1998 Manni Wood

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

  Manni Wood: mwood@sig.bsh.com, pq1036@110.net

 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>
#include <errno.h>
#include <glib.h>

/* read a line from the file handle filehandle, and return
   a pointer to an allocated null terminated array of char.
   Also, set success true or false depending on whether or
   not EOF was encountered while reading filehandle */
gchar *get_line_safely(FILE *filehandle, gboolean *success);


#endif /* ifndef GETLINE_H */

